var deactivate_user = true;
var create_user = true;

jQuery(document).ready(
    function () {
      if(window.location.href.indexOf('mo_drupal_to_okta') !== -1){
        deactivate_user_checkbox = document.getElementById("edit-okta-manual-provisioning-operations-operations-deactivate-user-okta");
        deactivate_user = !deactivate_user_checkbox.checked;
        create_user_checkbox = document.getElementById("edit-okta-manual-provisioning-operations-operations-create-user-okta");
        create_user = !create_user_checkbox.checked;
        setCookie();
      }

        function setCookie()
        {
            if((deactivate_user || create_user)) {
                if(deactivate_user && create_user) {
                      document.cookie = "checkBoxMessage=both unchecked";
                }else if(deactivate_user) {
                    document.cookie = "checkBoxMessage=Deactivate";
                }else if(create_user) {
                    document.cookie = "checkBoxMessage=Create"
                }
            }else{
                document.cookie = "checkBoxMessage=both checked";
            }
        }

        jQuery("#edit-okta-manual-provisioning-operations-operations-create-user-okta").change(
            function () {
                create_user = !this.checked;
                deactivate_user_checkbox = document.getElementById("edit-okta-manual-provisioning-operations-operations-deactivate-user-okta");
                deactivate_user = !deactivate_user_checkbox.checked;
                setCookie();
            }
        );

        jQuery("#edit-okta-manual-provisioning-operations-operations-deactivate-user-okta").change(
            function () {
                deactivate_user = !this.checked;
                create_user_checkbox = document.getElementById("edit-okta-manual-provisioning-operations-operations-create-user-okta");
                create_user = !create_user_checkbox.checked;
                setCookie();
            }
        );

    }
);
