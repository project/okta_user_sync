<?php

namespace Drupal\okta_user_sync\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for okta user sync module.
 */
class OktaUserSyncController extends ControllerBase {

  /**
   * Scim_clientController constructor.
   *
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function __construct() {
    $this->formBuilder = \Drupal::formBuilder();
  }

  /**
   * Opens Trial Request form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns ajaxresponse object.
   */
  public function openTrialRequestForm(): AjaxResponse {
    $response = new AjaxResponse();
    $modal_form = $this->formBuilder->getForm('\Drupal\okta_user_sync\Form\MoOktaRequestTrial');
    $response->addCommand(new OpenModalDialogCommand('Request 7-Days Full Feature Trial License', $modal_form, ['width' => '40%']));
    return $response;
  }

  /**
   * Opens support form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns ajaxresponse object.
   */
  public function openCustomerRequestForm(): AjaxResponse {
    $response = new AjaxResponse();
    $modal_form = $this->formBuilder->getForm('\Drupal\okta_user_sync\Form\MoOktaCustomerRequest');
    $response->addCommand(new OpenModalDialogCommand('Contact miniOrange Support', $modal_form, ['width' => '40%']));
    return $response;
  }

}
