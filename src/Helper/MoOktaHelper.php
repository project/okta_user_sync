<?php

namespace Drupal\okta_user_sync\Helper;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\okta_user_sync\MoOktaConstants;
use Drupal\user_provisioning\Helpers\moUserProvisioningLogger;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Helper class for okta user sync module.
 */
class MoOktaHelper {

  /**
   * ImmutableConfig property.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Config property.
   *
   * @var Drupal\Core\Config\Config
   */
  private Config $configFactory;

  /**
   * Messenger property.
   *
   * @var object
   */
  protected $messenger;

  /**
   * Base URL of the site.
   *
   * @var string
   */
  private $base_url;

  /**
   * URL Path property.
   *
   * @var string
   */
  private string $urlPath;

  /**
   * Logger property.
   *
   * @var Drupal\user_provisioning\Helpers\moUserProvisioningLogger
   */
  private moUserProvisioningLogger $moLogger;

  /**
   * The httpclient property.
   *
   * @var GuzzleHttp\Client
   */
  private Client $http_client;

  /**
   * The userinfo endpoint.
   *
   * @var string
   */
  private $userinfoEndpoint;

  /**
   * Constructs object of MoOktaHelper class.
   */
  public function __construct() {
    global $base_url;
    $this->base_url = $base_url;
    $this->urlPath = $base_url . '/' . \Drupal::service('extension.list.module')->getPath('okta_user_sync') . '/includes';
    $this->config = \Drupal::config('okta_user_sync.settings');
    $this->messenger = \Drupal::messenger();
    $this->configFactory = \Drupal::configFactory()->getEditable('okta_user_sync.settings');
    if ((\Drupal::service('module_handler')->moduleExists('user_provisioning'))) {
      $this->moLogger = new moUserProvisioningLogger();
    }

    $this->http_client = \Drupal::httpClient();

  }

  /**
   * Makes API call to Okta for getting user information.
   *
   * @param string $bearer_token
   *   The bearer token obtained from okta.
   * @param string $api_url
   *   The endpoint to make api call.
   *
   * @return false|string|void
   *   Returns api call response
   */
  public function getUserFromOkta($bearer_token, $api_url) {
    if (!$this->isCurlInstalled()) {
      return Json::encode(
            [
              "statusCode" => 'ERROR',
              "statusMessage" => 'cURL is not enabled on your site. Please enable the cURL module.',
            ]
        );
    }
    $options = ['headers' => ['Authorization' => 'SSWS ' . $bearer_token], 'verify' => FALSE];

    $this->moLogger->addLog('Query url is ' . $api_url, __LINE__, __FUNCTION__, __FILE__);
    $this->moLogger->addFormattedLog($options, __LINE__, __FUNCTION__, __FILE__, 'The header for testing the search request is:');
    try {
      // 409 conflict should be handled through the catch statement
      $response = \Drupal::httpClient()->get($api_url, $options);

    }
    catch (GuzzleException $exception) {

      if ($exception->getCode() == 0) {
        $error_msg = $exception->getMessage();
        $error_code = [
          "%error" => $error_msg,
          "%Description" => "Please Configure the fields correctly.",
        ];
        \Drupal::logger('user_provisioning')->notice('Error:  %error Cause: %Description', $error_code);
        $this->messenger->addError(t($error_msg));
        $this->configFactory
          ->set('mo_okta_attr_list_from_server', '')
          ->save();
        $response = new RedirectResponse($this->base_url . '/admin/config/people/okta_user_sync/overview?tab=drupal-to-okta-configuration');
        $response->send();
        exit();

      }
      else {
        $this->configFactory
          ->set('mo_okta_attr_list_from_server', '')
          ->save();
        $error = [
          '%error' => $exception->getResponse()->getBody()->getContents(),
        ];
        \Drupal::logger('user_provisioning')->notice('Error:  %error', $error);
        foreach ($error as $key => $value) {
          return $value;
        }
      }
    }
    $content = $response->getBody()->getContents();
    return $content;
  }

  /**
   * Creates url for testing.
   *
   * @param string $url
   *   The url.
   * @param string $upn
   *   The upn.
   *
   * @return string
   *   Returns trimmed url.
   */
  public static function creatUrlForTesting($url, $upn) {
    return rtrim($url, '/') . '/' . 'api' . '/' . 'v1' . '/' . 'users' . '/' . $upn;
  }

  /**
   * Fetches user attributes from Okta.
   */
  public function fetchAttributes() {
    $upn = $this->configFactory->get('okta_user_sync_upn');
    $bearer_token = $this->configFactory->get('okta_user_sync_bearer_token');
    $url = $this->configFactory->get('okta_user_sync_base_url');

    $api_url = self::creatUrlForTesting($url, $upn);
    $response = self::getUserFromOkta($bearer_token, $api_url);
    $content = json_decode($response);
    $user_details = $this->moOktaArrayFlattenAttributes($content);

    $user_details_encoded = Json::encode($user_details);

    $this->configFactory
      ->set('mo_okta_attr_list_from_server', $user_details_encoded)
      ->save();

  }

  /**
   * Checks curl is installed or not.
   *
   * @return bool
   *   Returns true if installed if true else false.
   */
  public function isCurlInstalled(): bool {
    return in_array('curl', get_loaded_extensions());
  }

  /**
   * Flattens user attributes received from Okta.
   *
   * @param array $details
   *   The user info array.
   *
   * @return array
   *   Returns flattened array
   */
  public function moOktaArrayFlattenAttributes($details): array {
    $arr = [];
    foreach ($details as $key => $value) {

      if (empty($value)) {
        continue;
      }
      if (!is_object($value)&& !is_array($value)) {
        $arr[$key] = filter_var($value);
      }
      else {
        $this->moOktaArrayFlattenAttributesLvl2($key, $value, $arr);
      }
    }

    return $arr;
  }

  /**
   * Seperates user attributes by | if nested.
   *
   * @param string $index
   *   Key of array.
   * @param array $arr
   *   The array.
   * @param array $haystack
   *   The array of flattened attributes.
   */
  private function moOktaArrayFlattenAttributesLvl2($index, $arr, &$haystack) {
    foreach ($arr as $key => $value) {
      if (empty($value)) {
        continue;
      }

      if (!is_object($value) && !is_array($value)) {

        if (!strpos(strtolower($index), 'error')) {
          $haystack[$index . "|" . $key] = $value;
        }

      }
      else {
        $this->moOktaArrayFlattenAttributesLvl2($index . "|" . $key, $value, $haystack);
      }
    }
  }

  /**
   * Shows customer support icon.
   */
  public function moOktaShowCustomerSupportIcon(array &$form, FormStateInterface $form_state) {
    $form['mo_okta_customer_support_icon'] = [
      '#markup' => t('<a class="use-ajax mo-bottom-corner"  href="CustomerSupportOkta"><img src="' . $this->urlPath . '/mo-customer-support.png" alt="test image"></a>'),
    ];
  }

  /**
   * @param  string $url
   *   The url of make api call.
   * @param  array $fields
   *   The body of api request.
   * @param  bool $header
   *   The header of api request.
   * @return false|string|void
   *   Returns api call response
   */
  public function callService($url, $fields, $header) {
    if (!$this->isCurlInstalled()) {
      return json_encode(
            [
              "statusCode" => 'ERROR',
              "statusMessage" => 'cURL is not enabled on your site. Please enable the cURL module.',
            ]
        );
    }
    $fieldString = is_string($fields) ? $fields : json_encode($fields);

    try {
      $response = \Drupal::httpClient()
        ->post(
            $url, [
              'body' => $fieldString,
              'allow_redirects' => TRUE,
              'http_errors' => FALSE,
              'decode_content' => TRUE,
              'verify' => FALSE,
              'headers' => $header,
            ]
        );
      return $response->getBody()->getContents();
    }
    catch (RequestException $exception) {
      $error = [
        '%apiName' => explode("moas", $url)[1],
        '%error' => $exception->getResponse()->getBody()->getContents(),
      ];
      \Drupal::logger('okta_user_sync')->notice('Error at %apiName of  %error', $error);
    }
  }

  /**
   * This function is written to get the timestamp.
   *
   * @return string
   *   Returns timestamp.
   */
  public static function getTimestamp() {
    $url = MoOktaConstants::GET_TIMESTAMP;
    $response = (new MoOktaHelper)->callService($url, [], []);

    if (empty($content)) {
      $currentTimeInMillis = round(microtime(TRUE) * 1000);
      $currentTimeInMillis = number_format($currentTimeInMillis, 0, '', '');
    }
    return empty($content) ? $currentTimeInMillis : $content;
  }

}
