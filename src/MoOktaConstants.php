<?php

namespace Drupal\okta_user_sync;

/**
 * Class for handling constants used throughout the project.
 */
class MoOktaConstants {
  const SUPPORT_QUERY_CONTENT = "?subject=Drupal Okta User Sync Module: Licensing Query&body=I want to enquire regarding the pricing of Drupal Okta User Sync module";
  const MODULE_LANDING_PAGE = 'https://plugins.miniorange.com/drupal-okta-user-provisioning';
  const SETUP_GUIDE = 'https://plugins.miniorange.com/drupal-okta-user-sync';

  // API calls to login.xecurify.com and Register/Login tab.
  const BASE_URL = 'https://login.xecurify.com';
  const GET_TIMESTAMP = self::BASE_URL . '/moas/rest/mobile/get-timestamp';
  const MO_NOTIFY_SEND = self::BASE_URL . '/moas/api/notify/send';
  const SUPPORT_EMAIL = 'drupalsupport@xecurify.com';
}
