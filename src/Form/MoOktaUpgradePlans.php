<?php

namespace Drupal\okta_user_sync\Form;

use Drupal\okta_user_sync\MoOktaConstants;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user_provisioning\moUserProvisioningConstants;
use Drupal\Core\Render\Markup;
use Drupal\okta_user_sync\Helper\MoOktaHelper;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class for upgrade plans.
 */
class MoOktaUpgradePlans extends FormBase {

  /**
   * Base URL of the site.
   *
   * @var string
   */
  private $base_url;

  /**
   * Constructs a new MoOktaUpgradePlans object.
   */
  public function __construct() {
    global $base_url;
    $this->base_url = $base_url;
  }

  /**
   * FormID of the form.
   *
   * @return string
   *   Returns formID of the form.
   */
  public function getFormId(): string {
    // @todo Implement getFormId() method.
    return 'MoOktaUpgradePlans';
  }

  /**
   * Builds upgrade plans form.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // @todo Implement buildForm() method.
    $form['markup_library'] = [
      '#attached' => [
        'library' => [
          "user_provisioning/user_provisioning.admin",
          'okta_user_sync/okta_user_sync.admin',
        ],
      ],
    ];
    $form['okta_to_drupal_back_button'] = [
      '#type' => 'submit',
      '#value' => t('&#11164;  Back '),
      '#attributes' => ['class' => ['button', 'button--danger']],
      '#submit' => ["::backFunUpgradePlanes"],
    ];

    $form['mo_user_provisioning_header'] = [
      '#markup' => '<div><div><br><div class="container-inline mo_user_provisioning_upgrade_background_note"><h1>&nbsp; UPGRADE PLANS </h1></div><br><br>',
    ];
    $features = [
      [Markup::create(t('<br><h1>FREE</h1><p class="user_provisioning_pricing_rate"><sup>$</sup> 0</p><a class="button" disabled>You are on this Plan</a><br><br>')), Markup::create(t('<br><h1>PREMIUM</h1><p class="user_provisioning_pricing_rate"><sup>$</sup> 599 <sup>*</sup></p> <a class="button button--primary" target="_blank" href="mailto:' . moUserProvisioningConstants::SUPPORT_EMAIL . MoOktaConstants::SUPPORT_QUERY_CONTENT . '">&#9993; Contact Us</a> <br><br>'))],
      [Markup::create(t('<h4>FEATURE LIST</h4>')), Markup::create(t('<h4>FEATURE LIST</h4>'))],
      [
      // Features of Free version.
        Markup::create(
          t(
              '<div class="mo_user_provisioning_feature_list">
                            <ul class="checkmark">
                                <li>Automatic / Real Time provisioning for Creating Users from Drupal to Okta</li>
                                <li>Manual / On-demand provisioning for Creating Users from Drupal to Okta</li>
                                <li>Basic Attribute Mapping (Username and Email)</li>
                                <li>Audits and Logs</li>
                            </ul>
                           </div>'
          )
        ),

      // Features of Premium version.
        Markup::create(
          t(
              '<br><h3>ALL THE FEATURES OF FREE </h3><h2> + </h2> <br>
                           <div class="mo_user_provisioning_feature_list">
                            <ul class="checkmark">
                                <li>Automatic / Real Time Provisioning for automatic User management</li>
                                <li>Manual / On-demand provisioning for all CRUD operations from Drupal to Okta and vice versa</li>
                                <li>Scheduler / Cron based Provisioning for all CRUD operations from Drupal to Okta</li>
                                <li>Advanced Attribute Mapping</li>
                                <li>Create Activated user in Okta</li>
                                <li>Enforce Strong Password</li>

                            </ul>
                           </div>'
          )
        ),
      ],
    ];

    $form['miniorange_oauth_login_feature_list'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#rows' => $features,
      '#size' => 3,
      '#attributes' => ['class' => ['mo_upgrade_plans_features mo_user_prov_feature_table']],
    ];

    $form['mo_user_provisioning_instance_note'] = [
      '#type' => 'fieldset',
      '#prefix' => '<br>',
    ];

    $form['mo_user_provisioning_instance_note']['miniorange_oauth_client_instance_based'] = [
      '#markup' => t(
          '<div class="mo_instance_note"><b>*</b> This module follows an <b>Instance and Subscription Based</b> licensing structure. The listed prices are for purchase of a single instance. If you are planning to use the module on multiple instances, you can check out the bulk purchase discount on our website.</div><br>
                        <div class="mo_user_provisioning_highlight_background"><b><u>What is an Instance:</u></b> A Drupal instance refers to a single installation of a Drupal site. It refers to each individual website where the module is active. In the case of multisite/subsite Drupal setup, each site with a separate database will be counted as a single instance. For e.g. If you have the dev-staging-prod type of environment then you will require 3 licenses of the module (with additional discounts applicable on pre-production environments).</div>'
      ),
    ];

    $form['markup_7'] = [
      '#markup' => "<br><div class='mo_instance_note'><b>Return Policy - </b><br><br>
        At miniOrange, we want to ensure you are 100% happy with your purchase. If the module you purchased is not working as advertised, and you've attempted to resolve any issues with our support team, which couldn't get resolved, we will refund the whole amount given that you have a raised a refund request within the first 10 days of the purchase. Please email us at <a href='mailto:drupalsupport@xecurify.com'>drupalsupport@xecurify.com</a> for any queries regarding the return policy.</div>",
    ];

    $okta_helper = new MoOktaHelper();
    $okta_helper->moOktaShowCustomerSupportIcon($form, $form_state);

    return $form;
  }

  /**
   * For redirecting user back to configuration tab.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Returns response object
   */
  public function backFunUpgradePlanes(): Response {
    $state = \Drupal::configFactory()->getEditable('okta_user_sync.settings')->get('okta_user_sync_status1');
    $response = new RedirectResponse($this->base_url . '/admin/config/people/okta_user_sync/' . $state);
    $response->send();
    return new Response();
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement submitForm() method.
  }

}
