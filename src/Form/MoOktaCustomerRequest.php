<?php

namespace Drupal\okta_user_sync\Form;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\okta_user_sync\MoOktaSupport;

/**
 * Class for sending support query.
 */
class MoOktaCustomerRequest extends FormBase {

  /**
   * ImmutableConfig property.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Messenger property.
   *
   * @var object
   */
  protected $messenger;

  /**
   * Constructs a new MoOktaCustomerRequest object.
   */
  public function __construct() {
    $this->config = \Drupal::config('user_provisioning.settings');
    $this->messenger = \Drupal::messenger();
  }

  /**
   * FormID of the form.
   *
   * @return string
   *   Returns formID of the form.
   */
  public function getFormId() {
    // @todo Implement getFormId() method.
    return 'MoOktaCustomerRequest';
  }

  /**
   * Builds customer query form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // @todo Implement buildForm() method.
    $form['#prefix'] = '<div id="modal_example_form1">';
    $form['#suffix'] = '</div>';
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['markup_library'] = [
      '#attached' => [
        'library' => [
          "okta_user_sync/okta_user_sync.admin",
        ],
      ],
    ];

    $user_email = $this->config->get('user_provisioning_customer_admin_email');

    $form['mo_okta_customer_support_email_address'] = [
      '#type' => 'email',
      '#title' => t('Email'),
      '#default_value' => $user_email,
      '#required' => TRUE,
      '#attributes' => ['placeholder' => t('Enter valid email'), 'style' => 'width:99%;margin-bottom:1%;'],
    ];

    $form['mo_okta_customer_support_method'] = [
      '#type' => 'select',
      '#title' => t('What are you looking for'),
      '#attributes' => ['style' => 'width:99%;height:30px;margin-bottom:1%;'],
      '#options' => [
        'I need Technical Support' => t('I need Technical Support'),
        'I want to Schedule a Setup Call/Demo' => t('I want to Schedule a Setup Call/Demo'),
        'I have Sales enquiry' => t('I have Sales enquiry'),
        'I have a custom requirement' => t('I have a custom requirement'),
        'My reason is not listed here' => t('My reason is not listed here'),
      ],
    ];

    $form['mo_okta_customer_support_query'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => t('How can we help you?'),
      '#attributes' => ['placeholder' => t('Describe your query here!'), 'style' => 'width:99%'],
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit '),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button--primary',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $form;

  }

  /**
   * Sends support query.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   returns ajax response object
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state): AjaxResponse {

    $form_values = $form_state->getValues();
    $response = new AjaxResponse();
    // If there are any form errors, AJAX replace the form.
    $email = $form_values['mo_okta_customer_support_email_address'];
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#modal_example_form1', $form));
    }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      \Drupal::messenger()->addMessage(t('The email address <b><i>' . $email . '</i></b> is not valid.'), 'error');
      $response->addCommand(new ReplaceCommand('#modal_example_form1', $form));
    }
    else {
      $email = $form_values['mo_okta_customer_support_email_address'];
      $support_for = $form_values['mo_okta_customer_support_method'];
      $query = $form_values['mo_okta_customer_support_query'];
      $query_type = 'Contact Support';

      $support = new MoOktaSupport($email, $support_for, $query, $query_type);
      $support_response = $support->sendSupportQuery();
      if($support_response){
        $this->messenger->addStatus(t('Support query successfully sent. We will get back to you shortly.'));
      }else{
        $this->messenger->addError(t('Error submitting the support query. Please send us your query at <a href="mailto:drupalsupport@xecurify.com">drupalsupport@xecurify.com</a>.'));
      }
      $response->addCommand(new RedirectCommand(Url::fromRoute('okta_user_sync.overview')->toString()));
    }
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement submitForm() method.
  }

}
