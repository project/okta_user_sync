<?php

namespace Drupal\okta_user_sync\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\okta_user_sync\Helper\MoOktaHelper;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class for handling Okta to Drupal provisioning.
 */
class MoOktaToDrupal extends FormBase {

  /**
   * Base URL of the site.
   *
   * @var string
   */
  private $base_url;

  /**
   * ImmutableConfig property.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Config property.
   *
   * @var Drupal\Core\Config\Config
   */
  protected $configFactory;

  /**
   * Messenger property.
   *
   * @var object
   */
  protected $messenger;

  /**
   * Constructs a new MoOktaToDrupal object.
   */
  public function __construct() {
    global $base_url;
    $this->base_url = $base_url;
    $this->config = \Drupal::config('okta_user_sync.settings');
    $this->configFactory = \Drupal::configFactory()->getEditable('okta_user_sync.settings');
    $this->messenger = \Drupal::messenger();
  }

  /**
   * FormID of the form.
   *
   * @return string
   *   Returns formID of the form.
   */
  public function getFormId() {
    return 'MoOktaToDrupal';
  }

  /**
   * Builds Okta to Drupal provisining form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['mo_okta_sync_add_css'] = [
      '#attached' => [
        'library' => [
          'okta_user_sync/okta_user_sync.admin',
          'okta_user_sync/okta_user_sync.okta_test',
          'okta_user_sync/okta_user_sync.add_trial_popup',
          'core/drupal.dialog.ajax',
        ],
      ],
    ];

    $form['mo_summary_okta_header_style'] = [
      '#markup' => t('<div class="mo_okta_header_container_summary">'),
    ];

    \Drupal::configFactory()->getEditable('okta_user_sync.settings')->set('okta_user_sync_status1', 'okta_to_drupal')->save();

    $this->oktaManualDrupalProvisioning($form, $form_state);
    $this->schedulerBasedProvisioning($form, $form_state);
    $this->mapping($form, $form_state);
    $this->additionalFeatures($form, $form_state);
    $form['okta_to_drupal_save_button'] = [
      '#type' => 'submit',
      '#value' => t('Save Settings'),
      '#button_type' => 'primary',
      '#disabled' => TRUE,
    ];
    $okta_handler = new MoOktaHelper();
    $okta_handler->moOktaShowCustomerSupportIcon($form, $form_state);
    return $form;
  }

  /**
   * Shows mapping configurations.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function mapping(array &$form, FormStateInterface $form_state) {
    $mo_premium_tag = '<a href = "okta_upgrade_plans" >[PREMIUM]</a>';
    $form['mo_okta_mapping_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mapping ') . $mo_premium_tag . '<hr>',
    ];
    $this->basicAttributeMapping($form, $form_state);
    $this->customAttributeMapping($form, $form_state);
    return $form;
  }

  /**
   * Shows basic mapping configurations.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function basicAttributeMapping(array &$form, FormStateInterface $form_state) {
    $usr = User::load(\Drupal::currentUser()->id());
    $usrVal = $usr->toArray();
    $custom_fields = [];

    foreach ($usrVal as $key => $value) {
      $custom_fields[$key] = $key;
    }

    $form['mo_okta_mapping_fieldset']['okta_attribute_mapping'] = [
      '#type' => 'fieldset',
      '#attributes' => ['style' => 'box-shadow: none'],
      '#title' => $this->t('Basic Attribute Mapping<hr>'),
      '#disabled' => TRUE,

    ];
    $username_attr = $this->config->get('okta_user_sync_basic_username_mapping');
    $username_attr = $username_attr ?? 'mail';
    $mail_attr = $this->config->get('okta_user_sync_basic_mail_mapping');
    $mail_attr = $mail_attr ?? 'mail';

    $form['mo_okta_mapping_fieldset']['okta_attribute_mapping']['okta_username_attribute'] = [
      '#type' => 'select',
      '#title' => t('Username Attribute'),
      '#attributes' => ['style' => 'width:75%', 'placeholder' => 'Enter Username attribute'],
      '#default_value' => $custom_fields[$username_attr],
      '#options' => $custom_fields,
      '#prefix' => '<div><table class="mo_basic_attribute_mapping_table"><tr><td>',
      '#suffix' => '</td>',
    ];

    $form['mo_okta_mapping_fieldset']['okta_attribute_mapping']['okta_email_attribute'] = [
      '#type' => 'select',
      '#title' => t('Email Attribute'),
      '#default_value' => $custom_fields[$mail_attr],
      '#options' => $custom_fields,
      '#attributes' => ['style' => 'width:75%', 'placeholder' => 'Enter Email attribute'],
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table></div>',
    ];

    return $form;
  }

  /**
   * Shows custom mapping configurations.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function customAttributeMapping(array &$form, FormStateInterface $form_state) {

    $usr = User::load(\Drupal::currentUser()->id());
    $usrVal = $usr->toArray();
    $custom_fields = [];

    foreach ($usrVal as $key => $value) {
      $custom_fields[$key] = $key;
    }

    $form['mo_okta_mapping_fieldset']['mo_okta_automatic_prov_custom_attribute_mapping'] = [
      '#type' => 'fieldset',
      '#title' => t('Custom Attribute Mapping') . '<hr>',
      '#attributes' => ['style' => 'box-shadow: none'],
    ];

    $form['mo_okta_mapping_fieldset']['mo_okta_automatic_prov_custom_attribute_mapping']['attribute_mapping_info'] = [
      '#markup' => '<div class="mo_okta_highlight_background">This feature allows you to map the user attributes from your Drupal to Okta as well as from Okta to Drupal.</div>',
    ];

    $form['mo_okta_mapping_fieldset']['mo_okta_automatic_prov_custom_attribute_mapping']['drupal_attr_name'] = [
      '#prefix' => '<div><table class="mo_custom_attribute_mapping_table"><tr><td>',
      '#title' => t('Drupal Attribute Name'),
      '#type' => 'select',
      '#options' => $custom_fields,
      '#disabled' => TRUE,
      '#attributes' => ['style' => 'width:100% !important;'],
      '#suffix' => '</td>',
    ];

    $form['mo_okta_mapping_fieldset']['mo_okta_automatic_prov_custom_attribute_mapping']['okta_attribute_name'] = [
      '#type' => 'textfield',
      '#id' => 'text_field1',
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#title' => t('Okta Attribute Name'),
      '#attributes' => ['style' => 'width:100%', 'placeholder' => 'Enter Okta Attribute Name'],
      '#required' => FALSE,
      '#disabled' => TRUE,
    ];

    $form['mo_okta_mapping_fieldset']['mo_okta_automatic_prov_custom_attribute_mapping']['okta_add_name'] = [
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#type' => 'button',
      '#disabled' => 'true',
      '#attributes' => ['style' => 'float: right;'],
      '#value' => '+',
    ];

    $form['mo_okta_mapping_fieldset']['mo_okta_automatic_prov_custom_attribute_mapping']['okta_sub_name'] = [
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table></div>',
      '#type' => 'button',
      '#disabled' => 'true',
      '#value' => '-',
    ];
    return $form;
  }

  /**
   * Saves configurations.
   *
   * @return array
   *   Returns array of form elements.
   */
  public function saveSummaryConfiguration(array $form, FormStateInterface $form_state) {
    $form_values = ($form_state->getValues())['mo_summary_okta_configuration_table'];
    $data = [
      'Okta Portal URL:' => 'okta_user_sync_base_url',
      'Okta Bearer Token:' => 'okta_user_sync_bearer_token',
      'Test Your Configuration<br>Enter user email:' => 'okta_user_sync_upn',
    ];
    foreach ($data as $key => $value) {
      $this->configFactory->set($value, $form_values[$key][$value])->save();
    }
    $bearer_token = $this->configFactory->get('okta_user_sync_bearer_token');
    $provider_base_url = $this->configFactory->get('okta_user_sync_base_url');
    \Drupal::configFactory()->getEditable('user_provisioning.settings')->set('mo_user_provisioning_configured_application', 'okta')
      ->set('mo_provider_specific_provisioning_base_url', $provider_base_url)
      ->set('mo_provider_specific_provisioning_api_token', $bearer_token)
      ->save();

    $okta_helper = new MoOktaHelper();
    $okta_helper->fetchAttributes();

    $okta_test_data = Json::decode($this->config->get('mo_okta_attr_list_from_server'));
    if ($okta_test_data != '') {
      if (isset($okta_test_data["errorCode"])) {
        $this->messenger->addError(t('An error occurred while performing test configuration. Please refer to <a href="#test_config_result">Test Configuration Result</a> for more information.'));
      }
      else {
        $this->messenger->addstatus(t('Test Configuration successful. You can check the complete list of attributes received from Okta by clicking on the link <a href="#test_config_result">HERE</a>.'));
      }
    }
    return $form;
  }

  /**
   * For returning back to configure overview tab.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Returns response object
   */
  public function backFunMoOktaToDrupal(array $form, FormStateInterface $form_state) {
    $status = $this->config->get('okta_user_sync_status');
    if ($status == 'overview') {
      $response = new RedirectResponse($this->base_url . '/admin/config/people/okta_user_sync/overview');
    }
    else {
      $response = new RedirectResponse($this->base_url . '/admin/config/people/okta_user_sync/overview?tab=' . $status);
    }
    $response->send();
    return new Response();

  }

  /**
   * Shows manual provisining configurations.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function oktaManualDrupalProvisioning(array &$form, FormStateInterface $form_state) {
    $mo_premium_tag = '<a href = "okta_upgrade_plans" >[PREMIUM]</a>';

    $form['mo_okta_provisioning_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('Manual Provisioning Configuration ') . $mo_premium_tag . '<hr>',
      '#attributes' => ['style' => 'box-shadow: none,', 'class' => ['field_set_label_css']],
    ];

    $form['mo_okta_provisioning_fieldset']['okta_provisioning_operations'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attributes' => ['class' => ['mo_okta_provisioning_operations_table']],

    ];

    $row = $this->moProvisioningOperations();
    $form['mo_okta_provisioning_fieldset']['okta_provisioning_operations']['operations'] = $row;

    $form['mo_okta_provisioning_fieldset']['create_user_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Sync Users ') . '<hr>',
      '#attributes' => ['style' => 'box-shadow: none'],
    ];

    $form['mo_okta_provisioning_fieldset']['create_user_fieldset']['mo_okta_drupal_username'] = [
      '#type' => 'textfield',
      '#attributes' => ['placeholder' => 'Search Okta username of user to sync'],
      '#autocomplete_route_name' => 'user_provisioning.autocomplete',
      '#disabled' => TRUE,
      '#prefix' => '<p class="mo_okta_highlight_background"><strong>Note:</strong> Search the username of user present in Okta to sync it to the Drupal.</p><div class="container-inline">',
    ];

    $form['mo_okta_provisioning_fieldset']['create_user_fieldset']['mo_okta_sync_button'] = [
      '#type' => 'submit',
      '#value' => t('Sync'),
      '#button_type' => 'primary',
      '#attributes' => ['class' => ['mo_okta_sync_button']],
      '#disabled' => TRUE,
    ];

    $form['mo_okta_provisioning_fieldset']['create_user_fieldset']['mo_okta_sync_all_button'] = [
      '#type' => 'submit',
      '#value' => t('Sync All Users'),
      '#button_type' => 'primary',
      '#attributes' => ['class' => ['mo_okta_sync_all_button']],
      '#disabled' => TRUE,
    ];

    return $form;

  }

  /**
   * Shows scheduler based provisioning configurations.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function schedulerBasedProvisioning(array &$form, FormStateInterface $form_state) {
    $mo_premium_tag = '<a href = "okta_upgrade_plans" >[PREMIUM]</a>';

    $form['mo_okta_scheduler_provisioning_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('Scheduler Based Provisioning Configuration ') . $mo_premium_tag . '<hr>',
      '#attributes' => ['class' => ['field_set_label_css']],
    ];

    $form['mo_okta_scheduler_provisioning_fieldset']['okta_provisioning_operations'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attributes' => ['class' => ['mo_okta_provisioning_operations_table']],

    ];

    $row = $this->moProvisioningOperations();
    $form['mo_okta_scheduler_provisioning_fieldset']['okta_provisioning_operations']['operations'] = $row;

    $form['mo_okta_scheduler_provisioning_fieldset']['mo_okta_scheduler_provisioning_configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configuration ') . '<hr>',
      '#attributes' => ['style' => 'box-shadow: none'],
    ];

    $form['mo_okta_scheduler_provisioning_fieldset']['mo_okta_scheduler_provisioning_configuration']['auto_creation'] = [
      '#type' => 'number',
      '#title' => t('Fetch All users details in next * minutes'),
      '#attributes' => ['style' => 'width: 45%'],
      '#disabled' => TRUE,

    ];

    return $form;
  }

  /**
   * Shows all provisining operations.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function moProvisioningOperations(): array {
    $row['read_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => t('Read user'),
      '#disabled' => TRUE,
      '#prefix' => '<div class="container-inline">',
    ];

    $row['create_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => t('Create user '),
      '#disabled' => TRUE,
    ];

    $row['update_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => t('Update user'),
      '#disabled' => TRUE,
    ];

    $row['deactivate_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => t('Deactivate user '),
      '#disabled' => TRUE,
    ];

    $row['delete_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => t('Delete user'),
      '#disabled' => TRUE,
    ];

    return $row;
  }

  /**
   * Shows mapping configurations.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function attributeMapping(array &$form, FormStateInterface $form_state) {

    $mo_premium_tag = '<a href = "okta_upgrade_plans" >[PREMIUM]</a>';

    $form['mo_okta_attribute_mapping_details'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => t('Mapping'),
    ];

    $form['mo_okta_attribute_mapping_details']['okta_custom_attribute_mapping'] = [
      '#type' => 'fieldset',
      '#title' => t('Custom Attribute Mapping ') . $mo_premium_tag . '<hr>',
    ];

    $form['mo_okta_attribute_mapping_details']['okta_custom_attribute_mapping']['attribute_mapping_info'] = [
      '#markup' => '<div class="mo_okta_highlight_background">This feature allows you to map the user attributes from your Drupal to Okta.</div>',
    ];

    $custom_fields = [];
    $usr = User::load(\Drupal::currentUser()->id());
    $usrVal = $usr->toArray();
    foreach ($usrVal as $key => $value) {
      $custom_fields[$key] = $key;
    }

    $form['mo_okta_attribute_mapping_details']['okta_custom_attribute_mapping']['okta_custom_attribute_mapping_table'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attributes' => ['class' => ['custom_attribute_mapping_table']],
    ];

    $row = $this->moOktaCustomAttributeMapTable($custom_fields);
    $form['mo_okta_attribute_mapping_details']['okta_custom_attribute_mapping']['okta_custom_attribute_mapping_table']['custom_mapping'] = $row;

    return $form;
  }

  /**
   * Shows attr map table.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function moOktaCustomAttributeMapTable($custom_fields): array {

    $row['okta_drupal_attr_name'] = [
      '#title' => t('Drupal Attribute Name'),
      '#type' => 'select',
      '#options' => $custom_fields,
      '#disabled' => TRUE,
    ];

    $row['okta_attribute_name'] = [
      '#type' => 'textfield',
      '#title' => t('Okta Attribute Name'),
      '#disabled' => TRUE,
    ];

    $row['okta_add_button'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#disabled' => TRUE,
      '#value' => '+',
    ];

    $row['okta_sub_button'] = [
      '#type' => 'submit',
      '#button_type' => 'danger',
      '#disabled' => TRUE,
      '#value' => '-',
    ];

    return $row;
  }

  /**
   * Shows attr map table.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function moOktaAttributeMapTable(): array {
    $row['username_attribute'] = [
      '#type' => 'select',
      '#title' => t('Username Attribute'),
      '#options' => [1 => 'name'],
      '#disabled' => TRUE,
    ];

    $row['email_attribute'] = [
      '#type' => 'select',
      '#title' => t('Email Attribute'),
      '#options' => [1 => 'mail'],
      '#disabled' => TRUE,
    ];

    return $row;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement submitForm() method.
  }
  /**
   * Shows additional features.
   *
   * @return array
   *   Returns array of form elements.
   */
  public function additionalFeatures(array &$form, FormStateInterface $form_state){

    $mo_premium_tag = '<a href = "okta_upgrade_plans" >[PREMIUM]</a>';

    $form['mo_okta_features_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('Additional Features ') . $mo_premium_tag . '<hr>',
      '#attributes' => ['class' => ['field_set_label_css']],
    ];


    $form['mo_okta_features_fieldset']['mo_okta_perform_action_on delete'] = array(
      '#title' => t('Perform the following action on Drupal user list when user is deleted from IDP:'),
      '#type' => 'radios',
      '#tree' => TRUE,
      '#options' => array(
        'NONE' => t('Do Nothing'),
        'DELETE' => t('Delete Users'),
        'ANONYMOUS' => t('Delete user and assign content to an anonymous user')
      ),
      '#disabled' => True
    );
  }

}
