<?php

namespace Drupal\okta_user_sync\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;

/**
 * Class for Mapping attributes.
 */
class MoMappingTab extends FormBase {

  /**
   * Base URL of the site.
   *
   * @var string
   */
  private $base_url;

  /**
   * ImmutableConfig property.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Config property.
   *
   * @var Drupal\Core\Config\Config
   */
  private Config $configfactory;

  /**
   * Messenger property.
   *
   * @var object
   */
  protected $messenger;

  /**
   * Constructs a new MoMappingTab object.
   */
  public function __construct() {
    global $base_url;
    $this->base_url = $base_url;
    $this->config = \Drupal::config('okta_user_sync.settings');
    $this->configfactory = \Drupal::configFactory()->getEditable('okta_user_sync.settings');
    $this->messenger = \Drupal::messenger();
  }

  /**
   * FormID of the form.
   *
   * @return string
   *   Returns formID of the form.
   */
  public function getFormId() {
    return 'MoMappingTab';
  }

  /**
   * Build Mapping form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    \Drupal::configFactory()->getEditable('okta_user_sync.settings')->set('okta_user_sync_status1', 'mo_mapping_tab')->save();

    $form['mo_okta_sync_add_css'] = [
      '#attached' => [
        'library' => [
          'okta_user_sync/okta_user_sync.admin',
          'okta_user_sync/okta_user_sync.okta_test',
          'core/drupal.dialog.ajax',
        ],
      ],
    ];
    $form['mo_summary_okta_header_style'] = [
      '#markup' => t('<div class="mo_okta_header_container_summary">'),
    ];

    if (($this->config->get('mo_okta_attr_list_from_server')) != '') {
      $okta_test_data = Json::decode($this->config->get('mo_okta_attr_list_from_server'));
    }
    if (isset($okta_test_data['id']) || isset($okta_test_data['errorCode'])) {

      $form['mapping_tab_attribute_button'] = [
        '#type' => 'link',
        '#title' => t('Attributes Received from Okta'),
        '#url' => Url::fromRoute('okta_user_sync.attributeListOfOkta'),
        '#attributes' => ['style' => ['float: right'], 'class' => ['button', 'button--primary', 'use-ajax', 'js-form-submit', 'form-submit'], 'data-dialog-type' => 'modal', 'data-dialog-options' => json_encode(['width' => '65%'])],
        '#limit_validation_errors' => [],
      ];
    }
    $mo_premium_tag = '<a href = "okta_upgrade_plans" >[PREMIUM]</a>';

    $form['mo_okta_attribute_mapping_details']['okta_attribute_mapping'] = [
      '#type' => 'fieldset',
      '#title' => t('Basic Attribute Mapping (Drupal to Okta)') . '<hr>',
      '#prefix' => '<br><br><br>',
    ];
    $custom_fields = [];
    $usr = User::load(\Drupal::currentUser()->id());
    $usrVal = $usr->toArray();
    foreach ($usrVal as $key => $value) {
      $custom_fields[$key] = $key;
    }
    $username_attr = $this->configfactory->get('okta_user_sync_basic_username_mapping');
    $mail_attr = $this->configfactory->get('okta_user_sync_basic_mail_mapping');

    $username_attr = $username_attr ?? 'mail';
    $mail_attr = $mail_attr ?? 'mail';

    $form['mo_okta_attribute_mapping_details']['okta_attribute_mapping']['okta_username_attribute'] = [
      '#type' => 'select',
      '#title' => t('Username Attribute'),
      '#attributes' => ['style' => 'width:30rem', 'placeholder' => 'Enter Username attribute'],
      '#default_value' => $custom_fields[$username_attr],
      '#disabled' => TRUE,
      '#options' => $custom_fields,
      '#prefix' => '<div><table class="mo_basic_attribute_mapping_table"><tr><td>',
      '#suffix' => '</td>',
    ];

    $form['mo_okta_attribute_mapping_details']['okta_attribute_mapping']['okta_email_attribute'] = [
      '#type' => 'select',
      '#title' => t('Email Attribute'),
      '#default_value' => $custom_fields[$mail_attr],
      '#options' => $custom_fields,
      '#disabled' => TRUE,
      '#attributes' => ['style' => 'width:30rem', 'placeholder' => 'Enter Email attribute'],
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table></div>',
    ];

    $form['mo_okta_attribute_mapping_details']['okta_attribute_mapping']['okta_basic_mapping_save_button'] = [
      '#type' => 'submit',
      '#value' => t('Save Configuration '),
      '#button_type' => 'primary',
      '#submit' => ['::saveBasicMappingConfiguration'],
    ];

    $form['mo_okta_attribute_mapping_details']['mo_okta_automatic_prov_custom_attribute_mapping'] = [
      '#type' => 'fieldset',
      '#title' => t('Custom Attribute Mapping') . ' ' . $mo_premium_tag . '<hr>',
    ];

    $form['mo_okta_attribute_mapping_details']['mo_okta_automatic_prov_custom_attribute_mapping']['attribute_mapping_info'] = [
      '#markup' => '<div class="mo_okta_highlight_background">This feature allows you to map the user attributes from your Drupal to Okta as well as from Okta to Drupal.</div>',
    ];

    $form['mo_okta_attribute_mapping_details']['mo_okta_automatic_prov_custom_attribute_mapping']['drupal_attr_name'] = [
      '#prefix' => '<div><table class="mo_custom_attribute_mapping_table"><tr><td>',
      '#title' => t('Drupal Attribute Name'),
      '#type' => 'select',
      '#options' => $custom_fields,
      '#disabled' => TRUE,
      '#attributes' => ['style' => 'width:100% !important;'],
      '#suffix' => '</td>',
    ];

    $form['mo_okta_attribute_mapping_details']['mo_okta_automatic_prov_custom_attribute_mapping']['okta_attribute_name'] = [
      '#type' => 'textfield',
      '#id' => 'text_field1',
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#title' => t('Okta Attribute Name'),
      '#attributes' => ['style' => 'width:100%', 'placeholder' => 'Enter Okta Attribute Name'],
      '#required' => FALSE,
      '#disabled' => TRUE,
    ];

    $form['mo_okta_attribute_mapping_details']['mo_okta_automatic_prov_custom_attribute_mapping']['okta_add_name'] = [
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#type' => 'button',
      '#disabled' => 'true',
      '#attributes' => ['style' => 'float: right;'],
      '#value' => '+',
    ];

    $form['mo_okta_attribute_mapping_details']['mo_okta_automatic_prov_custom_attribute_mapping']['okta_sub_name'] = [
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table></div>',
      '#type' => 'button',
      '#disabled' => 'true',
      '#value' => '-',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function saveBasicMappingConfiguration(array &$form, FormStateInterface $form_state) {
    $this->configfactory
      ->set('okta_user_sync_basic_username_mapping', $form_state->getValues()['okta_username_attribute'])
      ->set('okta_user_sync_basic_mail_mapping', $form_state->getValues()['okta_email_attribute'])
      ->save();
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement submitForm() method.
  }

}
