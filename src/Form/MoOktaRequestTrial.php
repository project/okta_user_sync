<?php

namespace Drupal\okta_user_sync\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\okta_user_sync\MoOktaSupport;
use Drupal\user_provisioning\moUserProvisioningConstants;

/**
 * Class for handling trial request.
 */
class MoOktaRequestTrial extends FormBase {

  /**
   * ImmutableConfig property.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Messenger property.
   *
   * @var object
   */
  protected $messenger;

  /**
   * Constructs a new MoOktaRequestTrial object.
   */
  public function __construct() {
    $this->config = \Drupal::config('user_provisioning.settings');
    $this->messenger = \Drupal::messenger();
  }

  /**
   * FormID of the form.
   *
   * @return string
   *   Returns formID of the form.
   */
  public function getFormId() {
    return 'mo_okta_request_trial';
    // @todo Implement getFormId() method.
  }

  /**
   * Builds trial request form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $options = NULL) {
    // @todo Implement buildForm() method.
    $form['#prefix'] = '<div id="modal_request_trial_form">';
    $form['#suffix'] = '</div>';
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['markup_library'] = [
      '#attached' => [
        'library' => [
          "okta_user_sync/okta_user_sync.admin",
        ],
      ],
    ];

    $user_email = $this->config->get('user_provisioning_customer_admin_email');

    $form['mo_okta_trial_email_address'] = [
      '#type' => 'email',
      '#title' => t('Email'),
      '#default_value' => $user_email,
      '#required' => TRUE,
      '#attributes' => ['placeholder' => t('Enter your email'), 'style' => 'width:99%;margin-bottom:1%;'],
    ];

    $form['mo_okta_trial_method'] = [
      '#type' => 'select',
      '#title' => t('Trial Method'),
      '#attributes' => ['style' => 'width:99%;height:30px;margin-bottom:1%;'],
      '#options' => [
        'Drupal to Okta CRUD operations' => t('Drupal to Okta Sync (Complete CRUD operations)'),
        'Okta to Drupal Sync' => t('Okta to Drupal Sync (Complete CRUD operations)'),
        'bidirectional' => t('Bidirectional sync (From Okta to Drupal or vice versa)'),
        'Not Sure' => t('Not Sure (We will assist you further)'),
      ],
    ];

    $form['mo_okta_trial_description'] = [
      '#type' => 'textarea',
      '#rows' => 4,
      '#required' => TRUE,
      '#title' => t('Description'),
      '#attributes' => ['placeholder' => t('Describe your use case here!'), 'style' => 'width:99%;'],
    ];

    $form['mo_okta_trial_note'] = [
      '#markup' => t('<div>If you are not sure what to choose, you can get in touch with us on <a href="mailto:' . moUserProvisioningConstants::SUPPORT_EMAIL . '">' . moUserProvisioningConstants::SUPPORT_EMAIL . '</a> and we will assist you further.</div>'),
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button--primary',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $form;
  }

  /**
   * Sends Trial Request.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns ajaxresponse object.
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();

    $response = new AjaxResponse();
    $email = $form_values['mo_okta_trial_email_address'];
    // If there are any form errors, AJAX replace the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#modal_request_trial_form', $form));
    }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      \Drupal::messenger()->addMessage(t('The email address <b><i>' . $email . '</i></b> is not valid.'), 'error');
      $response->addCommand(new ReplaceCommand('#modal_request_trial_form', $form));
    } else {
      $email = $form_values['mo_okta_trial_email_address'];
      $query = $form_values['mo_okta_trial_method'];
      $query_description = $form_values['mo_okta_trial_description'];
      $query_type = 'Trial Request';

      $support = new MoOktaSupport($email, $query, $query_description, $query_type);
      $support_response = $support->sendSupportQuery();

      if($support_response){
        $this->messenger->addStatus(t('Support query successfully sent. We will provide you with a trial very shortly.'));
      }else{
        $this->messenger->addError(t('Error submitting the support query. Please send us your query at <a href="mailto:drupalsupport@xecurify.com">drupalsupport@xecurify.com</a>.'));
      }
      $response->addCommand(new RedirectCommand(Url::fromRoute('okta_user_sync.overview')->toString()));
    }
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement submitForm() method.
  }

}
