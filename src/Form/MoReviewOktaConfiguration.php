<?php

namespace Drupal\okta_user_sync\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\okta_user_sync\Helper\MoOktaHelper;
use Drupal\user_provisioning\Helpers\moUserProvisioningLogger;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class for handling Okta configurations in the summary tab.
 */
class MoReviewOktaConfiguration extends FormBase {

  /**
   * Base URL of the site.
   *
   * @var string
   */
  private $base_url;

  /**
   * ImmutableConfig property.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Config property.
   *
   * @var Drupal\Core\Config\Config
   */
  protected $configFactory;

  /**
   * Messenger property.
   *
   * @var object
   */
  protected $messenger;

  /**
   * Logger property.
   *
   * @var Drupal\user_provisioning\Helpers\moUserProvisioningLogger
   */
  private moUserProvisioningLogger $moLogger;

  /**
   * Constructs a new MoReviewOktaConfiguration object.
   */
  public function __construct() {
    global $base_url;
    $this->base_url = $base_url;
    $this->config = \Drupal::config('okta_user_sync.settings');
    $this->configFactory = \Drupal::configFactory()->getEditable('okta_user_sync.settings');
    $this->messenger = \Drupal::messenger();
    $this->moLogger = new moUserProvisioningLogger();
  }

  /**
   * FormID of the form.
   *
   * @return string
   *   Returns formID of the form.
   */
  public function getFormId() {
    return 'review_okta_configuration_popup';
  }

  /**
   * Builds review okta configurations form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#prefix'] = '<div id="review_okta_configuration">';
    $form['#suffix'] = '</div>';
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['markup_library'] = [
      '#attached' => [
        'library' => [
          "okta_user_sync/okta_user_sync.admin",
          'okta_user_sync/okta_user_sync.close_review_okta_configuration',
        ],
      ],
    ];

    $form['mo_con_okta_header_style'] = [
      '#markup' => t('<div class="mo_okta_header_container_step1">'),
    ];

    $tableVar = new MoOktaOverview();
    $tableVar->oktaConfigurationTable($form, $form_state, 'summary');

    if (!empty($this->config->get('okta_user_sync_base_url')) && !empty($this->config->get('okta_user_sync_bearer_token')) && !empty($this->config->get('okta_user_sync_upn'))) {
      $result = $this->testAttributesFromOkta();
      $form['okta_test_config_result'] = $result;
    }

    $form['attribute_table_div_close'] = [
      '#markup' => '</div>',
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save and Test Configuration'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button--primary',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'saveReviewOktaConfig'],
        'event' => 'click',
      ],
    ];
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $form;
  }

  /**
   * Saves Okta configuration and performs test configuration.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Returns response object
   */
  public function saveReviewOktaConfig(array $form, FormStateInterface $form_state) {

    $form_values = ($form_state->getValues())['mo_summary_okta_configuration_table'];

    $data = [
      'Okta Portal URL:' => 'okta_user_sync_base_url',
      'Okta Bearer Token:' => 'okta_user_sync_bearer_token',
      'Test Your Configuration<br>Enter user email:' => 'okta_user_sync_upn',
    ];

    foreach ($data as $key => $value) {
      $this->configFactory->set($value, $form_values[$key][$value])->save();
    }

    $bearer_token = $this->configFactory->get('okta_user_sync_bearer_token');
    $provider_base_url = $this->configFactory->get('okta_user_sync_base_url');
    \Drupal::configFactory()->getEditable('user_provisioning.settings')->set('mo_user_provisioning_configured_application', 'okta')
      ->set('mo_provider_specific_provisioning_base_url', $provider_base_url)
      ->set('mo_provider_specific_provisioning_api_token', $bearer_token)
      ->save();

    self::fetchAttributesFromOkta($form, $form_state);

    $okta_test_data = Json::decode($this->config->get('mo_okta_attr_list_from_server'));

    foreach ($data as $key => $value) {
      $form['mo_okta_summary_configuration']['mo_summary_okta_configuration_table'][$key][$value]['#value'] = $form_values[$key][$value];
    }
    if ($okta_test_data == '') {
      $form['okta_test_config_result'] = NULL;
    }

    if ($okta_test_data != '') {
      if (isset($okta_test_data["errorCode"])) {
        $this->messenger->addError(t('An error occurred while performing test configuration. Please refer to <a href="#test_config_result">Test Configuration Result</a> for more information.'));
      }
      else {
        $this->messenger->addstatus(t('Test Configuration successful. You can check the complete list of attributes received from Okta by clicking on the link <a href="#test_config_result">HERE</a>.'));
      }
    }
    else {
      $this->messenger->addError('Something went wrong. Please check your configurations.');
    }

    $response = new AjaxResponse();
    $result = $this->testAttributesFromOkta();

    $response->addCommand(new ReplaceCommand('#review_okta_configuration', $form));
    $response->addCommand(new ReplaceCommand('#test_config_result', $result));

    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement submitForm() method.
  }

  /**
   * Shows test configuration result.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function testAttributesFromOkta() {
    $okta_test_data = '';
    if (($this->config->get('mo_okta_attr_list_from_server')) != '') {
      $okta_test_data = Json::decode($this->config->get('mo_okta_attr_list_from_server'));
    }

    $form['mo_okta_summary_configuration']['okta_configuration_display_test_data'] = [
      '#type' => 'details',
      '#title' => $this->t('Test Configuration Result'),
      '#open' => isset($okta_test_data["errorCode"]) ,
      '#prefix' => '<div id="test_config_result">',
    ];

    $status_message = '';
    if ($okta_test_data != '') {
      $status_message = isset($okta_test_data["errorCode"]) ?
            $this->t(
            '<div style="font-family:Calibre,serif;padding:0 3%;">
                <div style="color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 20px;text-align:center;border:1px solid #E6B3B2;font-size:18pt;">
                    ERROR
                </div><div style="color: #a94442;font-size:14pt; margin-bottom:20px;">'
        ) :
            $this->t(
            '<div style="font-family:Calibre,serif;padding:0 3%;"><div style="width:95%;color: #3c763d;background-color: #dff0d8;padding: 2%;margin-bottom: 20px;text-align: center;border: 1px solid #AEDB9A;font-size: 18pt;">
                    SUCCESS
                </div>'
        );
    }
    else {
      $status_message = $this->t(
            '<div style="font-family:Calibre,serif;padding:0 3%;">
                <div style="color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 20px;text-align:center;border:1px solid #E6B3B2;font-size:18pt;">
                    ERROR
                </div><div style="color: #a94442;font-size:14pt; margin-bottom:20px;">'
        );
    }

    $form['mo_okta_summary_configuration']['okta_configuration_display_test_data']['header_message'] = [
      '#markup' => $status_message,
    ];

    $form['mo_okta_summary_configuration']['okta_configuration_display_test_data']['okta_configuration_show_test_data'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attributes' => ['style' => 'border-collapse: separate;', 'class' => ['mo_okta_test_data']],
    ];

    if ($okta_test_data != NULL) {
      foreach ($okta_test_data as $key => $value) {
        $test_result = new MoOktaOverview();
        $row = $test_result->oktaTestConfigurationData($key, $value);
        $form['mo_okta_summary_configuration']['okta_configuration_display_test_data']['okta_configuration_show_test_data'][$key] = $row;
      }
    }

    return $form;
  }

  /**
   * Fetches attributes from Okta.
   */
  public function fetchAttributesFromOkta(array &$form, FormStateInterface $form_state) {
    $upn = $this->configFactory->get('okta_user_sync_upn');
    $bearer_token = $this->configFactory->get('okta_user_sync_bearer_token');
    $url = $this->configFactory->get('okta_user_sync_base_url');
    $okta_helper = new MoOktaHelper();

    $api_url = $okta_helper->creatUrlForTesting($url, $upn);
    $response = self::getUserFromOktaInPopup($bearer_token, $api_url, $form);
    $content = json_decode($response);
    if ($content != NULL) {
      $user_details = $okta_helper->moOktaArrayFlattenAttributes($content);
      $user_details_encoded = Json::encode($user_details);
      $this->configFactory
        ->set('mo_okta_attr_list_from_server', $user_details_encoded)
        ->save();
    }
    else {
      $this->configFactory
        ->set('mo_okta_attr_list_from_server', '')
        ->save();
    }
  }

  /**
   * Makes API call to okta.
   *
   * @param string $bearer_token
   *   The bearer token from Okta.
   * @param string $api_url
   *   The endpoint.
   * @param array $form
   *   The array of form elements.
   *
   * @return string|Drupal\Core\Ajax\AjaxResponse
   *   Returns api call response
   */
  public function getUserFromOktaInPopup($bearer_token, $api_url, array &$form,) {
    $okta_helper = new MoOktaHelper();
    if (!$okta_helper->isCurlInstalled()) {
      return Json::encode(
            [
              "statusCode" => 'ERROR',
              "statusMessage" => 'cURL is not enabled on your site. Please enable the cURL module.',
            ]
        );
    }
    $options = ['headers' => ['Authorization' => 'SSWS ' . $bearer_token], 'verify' => FALSE];

    $this->moLogger->addLog('Query url is ' . $api_url, __LINE__, __FUNCTION__, __FILE__);
    $this->moLogger->addFormattedLog($options, __LINE__, __FUNCTION__, __FILE__, 'The header for testing the search request is:');
    try {
      // 409 conflict should be handled through the catch statement
      $response = \Drupal::httpClient()->get($api_url, $options);

    }
    catch (GuzzleException $exception) {

      if ($exception->getCode() == 0) {
        $error_msg = $exception->getMessage();
        $error_code = [
          "%error" => $error_msg,
          "%Description" => "Please Configure the fields correctly.",
        ];
        \Drupal::logger('user_provisioning')->notice('Error:  %error Cause: %Description', $error_code);
        $this->messenger->addError(t($error_msg));
        $this->configFactory
          ->set('mo_okta_attr_list_from_server', '')
          ->save();

        $response = new AjaxResponse();
        $result = $this->testAttributesFromOkta();

        $response->addCommand(new ReplaceCommand('#review_okta_configuration', $form));
        $response->addCommand(new ReplaceCommand('#test_config_result', $result));

        return $response;

      }
      else {
        $error = [
          '%error' => $exception->getResponse()->getBody()->getContents(),
        ];
        \Drupal::logger('user_provisioning')->notice('Error:  %error', $error);
        foreach ($error as $key => $value) {
          return $value;
        }
      }
    }
    $content = $response->getBody()->getContents();
    return $content;
  }

}
