<?php

namespace Drupal\okta_user_sync\Form;

use Drupal\okta_user_sync\Helper\MoOktaHelper;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Render\Markup;

/**
 * Shows user provisioning configuration step by step.
 */
class MoOktaOverview extends FormBase {

  /**
   * Base URL of the site.
   *
   * @var string
   */
  private $base_url;

  /**
   * ImmutableConfig property.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Config property.
   *
   * @var Drupal\Core\Config\Config
   */
  protected $configFactory;

  /**
   * Config property.
   *
   * @var Drupal\Core\Config\Config
   */
  protected $messenger;

  /**
   * URL Path property.
   *
   * @var string
   */
  private $urlPath;

  /**
   * Constructs a new MoOktaOverview object.
   */
  public function __construct() {
    global $base_url;
    $this->base_url = $base_url;
    $this->urlPath = $base_url . '/' . \Drupal::service('extension.list.module')->getPath('okta_user_sync') . '/includes';
    $this->config = \Drupal::config('okta_user_sync.settings');
    $this->configFactory = \Drupal::configFactory()->getEditable('okta_user_sync.settings');
    $this->messenger = \Drupal::messenger();
  }

  /**
   * FormID of the form.
   *
   * @return string
   *   Returns formID of the form.
   */
  public function getFormId() {
    return "mo_okta_overview";
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['mo_okta_sync_css'] = [
      '#attached' => [
        'library' => [

          'okta_user_sync/okta_user_sync.admin',
          'okta_user_sync/okta_user_sync.okta_test',
          'core/drupal.dialog.ajax',
        ],
      ],
    ];
    if (!(\Drupal::service('module_handler')->moduleExists('user_provisioning'))) {
      \Drupal::service('module_installer')->install(['user_provisioning']);
    }

    if (!isset($_GET['tab'])) {
      $last_step = $this->configFactory->get('okta_user_sync_status1');
      $start_step = $this->configFactory->get('okta_user_sync_status');

      if (($last_step == 'overview' || $last_step == NULL) || $start_step == 'overview') {
        $this->oktaSyncOverview($form, $form_state);
      }
      else {
        if ($last_step == 'overview?tab=drupal-to-okta-configuration') {
          $this->step1Configuration($form, $form_state);
        }
        elseif ($last_step == 'overview?tab=how-to-perform-sync') {
          $this->step2SyncOption($form, $form_state);
        }
        elseif ($last_step == 'overview?tab=provisioning-configuration') {
          $this->step3TypesOfProvisioning($form, $form_state);
        }
        elseif ($last_step == 'okta_to_drupal') {
          $response = new RedirectResponse(Url::fromRoute('okta_user_sync.okta_to_drupal')->toString());
          $response->send();
          return new Response();
        }
        elseif ($last_step == 'mo_drupal_to_okta') {
          $response = new RedirectResponse(Url::fromRoute('okta_user_sync.mo_drupal_to_okta')->toString());
          $response->send();
          return new Response();
        }
        elseif ($last_step == 'mo_mapping_tab') {
          $response = new RedirectResponse(Url::fromRoute('okta_user_sync.mo_mapping_tab')->toString());
          $response->send();
          return new Response();
        }
        elseif ($last_step == 'MoOktaAuditLogs') {
          $response = new RedirectResponse(Url::fromRoute('okta_user_sync.okta_audit_logs')->toString());
          $response->send();
          return new Response();
        }
      }
    }
    else {
      if (isset($_GET['tab']) &&  strcmp($_GET['tab'], 'drupal-to-okta-configuration') == 0) {
        $this->step1Configuration($form, $form_state);

      }
      elseif (isset($_GET['tab']) && strcmp($_GET['tab'], 'how-to-perform-sync') == 0) {
        $this->step2SyncOption($form, $form_state);

      }
      elseif (isset($_GET['tab']) && strcmp($_GET['tab'], 'provisioning-configuration') == 0) {
        $this->step3TypesOfProvisioning($form, $form_state);

      }
    }

    $okta_handler = new MoOktaHelper();
    $okta_handler->moOktaShowCustomerSupportIcon($form, $form_state);

    return $form;
  }

  /**
   * Shows step3 of configuration.
   *
   * @return array
   *   Return form elements.
   */
  public function step3TypesOfProvisioning(array &$form, FormStateInterface $form_state): array {
    $this->configFactory->set('okta_user_sync_status', 'provisioning-configuration')->save();
    $this->configFactory->set('okta_user_sync_status1', 'overview?tab=provisioning-configuration')->save();

    $form['mo_okta_provisioning_type_header_style'] = [
      '#markup' => t('<div class="mo_okta_header_container_step1">'),
    ];

    $form['header_setup_guide_button_provisioning_type'] = [
      '#markup' => '<div class="container-inline"><a class="button button--primary mo-guides-floating" target="_blank" href="https://plugins.miniorange.com/drupal-okta-user-sync">🕮 Setup guide</a></div> ',
    ];

    $form['header_top_style_provisioning_type'] = [
      '#markup' => '<h2><u>STEP 3/3</u></h2><br><h3>Types of Provisioning</h3><hr><br>',
    ];

    $form['mo_okta_provisioning_type_table'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attributes' => ['class' => ['mo_okta_provisioning_types']],
    ];

    $row = $this->moTableDataProvisioningType();
    $form['mo_okta_provisioning_type_table'][] = $row;

    $form['configure_okta_back_button'] = [
      '#type' => 'submit',
      '#value' => t('&#11164; Back'),
      '#button_type' => 'danger',
      '#submit' => ["::backBtnProvisioningType"],
      '#prefix' => '<div class="mo-table-button-center">',
    ];

    $form['configure_okta_next_button'] = [
      '#type' => 'submit',
      '#value' => t("All done!!"),
      '#button_type' => 'primary',
      '#submit' => ["::performSyncFunction"],
      '#attributes' => ['style' => 'float:right'],

    ];
    return $form;
  }

  /**
   * For redirecting user to drupal to okta configuration page.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Returns response object
   */
  public function performSyncFunction() {
    $response = new RedirectResponse($this->base_url . '/admin/config/people/okta_user_sync/mo_drupal_to_okta');
    $response->send();
    return new Response();
  }

  /**
   * Shows step2 of configuration.
   *
   * @return array
   *   Return form elements.
   */
  public function step2SyncOption(array &$form, FormStateInterface $form_state): array {
    $this->configFactory->set('okta_user_sync_status1', 'overview?tab=how-to-perform-sync')->save();
    $form['mo_okta_provisioning_type_header_style'] = [
      '#markup' => t('<div class="mo_okta_header_container_step1">'),
    ];

    $form['header_setup_guide_button_provisioning_type'] = [
      '#markup' => '<div class="container-inline"><a class="button button--primary mo-guides-floating" target="_blank" href= "https://plugins.miniorange.com/drupal-okta-user-sync">🕮 Setup guide</a></div> ',
    ];

    $form['header_top_style_provisioning_type'] = [
      '#markup' => '<h2><u>STEP 2/3</u></h2><br><h3 class="mo_okta_text_center">Please select how would you like to perform the synchronization</h3><br>',
    ];

    $this->configFactory->set('okta_user_sync_status', 'how-to-perform-sync')->save();

    $rows = [
      [Markup::create('<a href="' . $this->base_url . '/admin/config/people/okta_user_sync/overview?tab=provisioning-configuration"><div class="mo_okta_step_1_img mo_okta_text_center"><img class="mo_okta_sync_gif" src="' . $this->urlPath . '/Drupal_to_Okta.gif" alt="drupal_to_okta"></div></a>'), Markup::create('<a class="js-form-submit form-submit use-ajax mo_top_bar_button" data-dialog-type="modal" href="requestTrialOkta"><div class="mo_okta_step_1_img mo_okta_text_center"><img class="mo_okta_sync_gif" src="' . $this->urlPath . '/Okta_to_Drupal.gif" alt="okta_to_drupal"></div></a>')],
    ];

    $form['okta_sync_how_to_sync'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#rows' => $rows,
      '#attributes' => ['style' => 'border-collapse: separate;', 'class' => ['step1_how_to_sync']],
    ];

    $form['okta_sync_back_button'] = [
      '#type' => 'submit',
      '#value' => t('&#11164; Back'),
      '#button_type' => 'danger',
      '#submit' => ["::backBtnHowToSync"],
    ];

    $form['okta_sync_header_end'] = [
      '#markup' => t('</div>'),
    ];

    return $form;
  }

  /**
   * Configuration for all types of provisioning.
   *
   * @return array
   *   Returns array of form elements.
   */
  public function moTableDataProvisioningType(): array {
    $row['okta_sync_manual_provisioning_button'] = [
      '#type' => 'link',
      '#title' => t('&#9881; Configure '),
      '#url' => Url::fromRoute('okta_user_sync.manualConfiguration'),
      '#attributes' => ['style' => ['margin-left:33%'], 'class' => ['button', 'button--primary', 'use-ajax', 'js-form-submit', 'form-submit'] , 'data-dialog-type' => 'modal', 'data-dialog-options' => json_encode(['width' => 'auto'])],
      '#prefix' => '<div class="mo_okta_sync_text_center"><h4>Manual/On-Demand Provisioning</h4></div><hr>In this method users can be manually provisioned to Okta whenever it required.<br>It allows provisioning of the single user as well as sync all the existing user in the Drupal.<br><br>',
    ];

    $row['okta_sync_automatic_provisioning_button'] = [
      '#type' => 'link',
      '#title' => t('&#9881; Configure '),
      '#url' => Url::fromRoute('okta_user_sync.automaticConfiguration'),
      '#attributes' => ['style' => ['margin-left:33%'], 'class' => ['button', 'button--primary', 'use-ajax', 'js-form-submit', 'form-submit'] , 'data-dialog-type' => 'modal', 'data-dialog-options' => json_encode(['width' => '65%'])],

      '#prefix' => '<div class="mo_okta_sync_text_center"><h4>Automatic/Real-time Provisioning</h4></div><hr>This methods allows the real-time synchronization of the users form Drupal to Okta.<br>Users are provisioned to Okta as soon as any CRUD operation is performed on the Drupal site.<br><br>',
    ];

    $row['okta_sync_scheduler_provisioning_button'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => t('&#9881;  Configure'),
      '#attributes' => ['style' => 'margin-left:33%;'],
      '#disabled' => TRUE,
      '#prefix' => '<div class="mo_okta_sync_text_center"><h4>Scheduler based Provisioning <a href="okta_upgrade_plans"><img class="scim_client_know_more" src="' . $this->urlPath . '/crown.png" alt="Premium"><span class="scim_client_know_more_hidden">Available in the Premium version</span></a></h4></div><hr>In this method, you can schedule the provisioning of the users after particular interval.<br>It can be based on the CRON of the system. Also, you can schedule your own time to provision users.<br><br>',
    ];

    return $row;
  }

  /**
   * For returning to how to perform sync page.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Returns response object
   */
  public function backBtnProvisioningType(array &$form, FormStateInterface $form_state) {
    $this->configFactory->set('okta_user_sync_status', 'how-to-perform-sync')->save();
    $response = new RedirectResponse($this->base_url . '/admin/config/people/okta_user_sync/overview?tab=how-to-perform-sync');
    $response->send();
    return new Response();
  }

  /**
   * Shows okta configuration table.
   *
   * @return array
   *   Return form elements.
   */
  public function oktaConfigurationTable(array &$form, FormStateInterface $form_state, $status = '') {
    if ($status == 'configuration') {
      $form['mo_configure_okta_configuration_table'] = [
        '#type' => 'table',
        '#responsive' => TRUE,
        '#attributes' => ['style' => 'border-collapse: separate;'],
      ];
    }
    elseif ($status == 'summary') {
      $form['mo_okta_summary_configuration']['mo_summary_okta_configuration_table'] = [
        '#type' => 'table',
        '#responsive' => TRUE,
        '#attributes' => ['style' => 'border-collapse: separate;'],
      ];
    }
    $data = [
      'Okta Portal URL:' => 'okta_user_sync_base_url',
      'Okta Bearer Token:' => 'okta_user_sync_bearer_token',
      'Test Your Configuration<br>Enter user email:' => 'okta_user_sync_upn',
    ];
    $description = [
      'Okta Portal URL:' => t("Enter the site url of your Okta Portal e.g https://xxxxxx.okta.com"),
      'Okta Bearer Token:' => t('You can create Token under <strong>Security &#8594; API &#8594; Tokens </strong>in your Okta portal.'),
      'Test Your Configuration<br>Enter user email:' => t('Enter Email of user which is present in Okta.'),
    ];

    if ($status == 'configuration') {
      foreach ($data as $key => $value) {
        $row = $this->moConfigurationTable($key, $value, $description);
        $form['mo_configure_okta_configuration_table'][$key] = $row;
      }
    }
    elseif ($status == 'summary') {
      foreach ($data as $key => $value) {
        $row = $this->moConfigurationTable($key, $value, $description);
        $form['mo_okta_summary_configuration']['mo_summary_okta_configuration_table'][$key] = $row;
      }
    }
    return $form;
  }

  /**
   * Shows configuration table.
   *
   * @return array
   *   returns array of configurations form elements
   */
  private function moConfigurationTable($key, $value, $description_field): array {
    $row[$key . $key] = [
      '#markup' => '<div class="container-inline"><strong> ' . $key . '</strong>',
    ];

    if (strcmp($key, 'Okta Portal URL:') == 0) {
      $row[$value] = [
        '#type' => 'url',
        '#maxlength' => 1048,
        '#required' => TRUE,
        '#default_value' => $this->configFactory->get('okta_user_sync_base_url'),
        '#description' => $description_field[$key],
        '#suffix' => '</div>',
      ];
    }
    elseif (strcmp($key, 'Okta Bearer Token:') == 0) {
      $row[$value] = [
        '#type' => 'textfield',
        '#maxlength' => 1048,
        '#required' => TRUE,
        '#value' => $this->configFactory->get('okta_user_sync_bearer_token'),
        '#description' => $description_field[$key],
        '#suffix' => '</div>',
      ];
    }
    elseif ($key == 'Test Your Configuration<br>Enter user email:') {
      $row[$value] = [
        '#type' => 'email',
        '#maxlength' => 1048,
        '#required' => TRUE,
        '#default_value' => $this->configFactory->get('okta_user_sync_upn'),
        '#description' => $description_field[$key],
        '#suffix' => '</div>',
      ];
    }
    return $row;
  }

  /**
   * For returning to overview page.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Returns response object
   */
  public function backFun(array &$form, FormStateInterface $form_state) {
    $this->configFactory->set('okta_user_sync_status1', 'overview')->save();
    $response = new RedirectResponse($this->base_url . '/admin/config/people/okta_user_sync/overview');
    $response->send();
    return new Response();
  }

  /**
   * For redirecting user to how to perform sync page.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Returns array of response object
   */
  public function nextBtnFun(array &$form, FormStateInterface $form_state) {
    $this->configFactory->set('okta_user_sync_status', 'how-to-perform-sync')->save();
    $response = new RedirectResponse($this->base_url . '/admin/config/people/okta_user_sync/overview?tab=how-to-perform-sync');
    $response->send();
    return new Response();
  }

  /**
   * Shows landing page of module.
   *
   * @return array
   *   Return form elements.
   */
  public function oktaSyncOverview(array &$form, FormStateInterface $form_state): array {
    $this->configFactory->set('okta_user_sync_status1', 'overview')->save();
    $this->configFactory->set('okta_user_sync_status', 'overview')->save();

    $form['overview_header_style'] = [
      '#markup' => t('<div class="mo_okta_header_container">'),
    ];
    $current_logged_in_user = \Drupal::currentUser()->getDisplayName();
    $current_logged_in_user = !empty($current_logged_in_user) ? $current_logged_in_user : 'there';
    $form['overview_left_side_content'] = [
      '#markup' => t(
          '<div class="mo_okta_header_container_left">
                  <h3>Let\'s Get Started!</h3>
                  <p>Hello ' . $current_logged_in_user . ', Thank you for installing <strong>Okta User Sync and Management</strong> module! <br><br>This Module provides you a bidirectional user synchronization and provisioning between the Okta and your Drupal site. The module supports User Provisioning from both Drupal to Okta as well as Okta to Drupal.
                  <br><br>Feel free to contact us at <a href="mailto:drupalsupport@xecurify.com">drupalsupport@xecurify.com</a> if you need any sort of assistance during the setup.</p>'
      ),
    ];

    $referer = "https://plugins.miniorange.com/drupal-okta-user-sync";

    $form['overview_left_side_guide_button'] = [
      '#type' => 'link',
      '#title' => t('&#128366; Setup Guide'),
      '#url' => Url::fromUri($referer),
      '#attributes' => ['class' => ['button', 'button--primary'], 'target' => '_blank'],
      '#prefix' => '<div class="container-inline">',
    ];

    $form['overview_left_side_configure-button'] = [
      '#type' => 'submit',
      '#value' => t('&#9881; &nbsp; Let\'s configure the module &nbsp; &#11166;'),
      '#button_type' => 'primary',
      '#submit' => ["::letsConfigureButton"],
      '#prefix' => '&nbsp;&nbsp;&nbsp;',
      '#suffix' => '</div>',
    ];

    $form['overview_left_side_content_end'] = [
      '#markup' => '</div>',
    ];

    $form['overview_right_side_content'] = [
      '#markup' => t(
          '<div class="mo_okta_header_container_right"><div> <a href="https://plugins.miniorange.com/drupal-okta-user-sync"  target="_blank" >
                  <img class="mo_okta_header_container_right_video" src="' . $this->urlPath . '/video_thumbnil.png" alt="setup_video_image"> </a>
                  </div>'
      ),
    ];
    return $form;
  }

  /**
   * Shows step1 configurations.
   *
   * @return array
   *   Returns array of form elements.
   */
  public function step1Configuration(array &$form, FormStateInterface $form_state): array {
    $this->configFactory->set('okta_user_sync_status', 'drupal-to-okta-configuration')->save();
    $this->configFactory->set('okta_user_sync_status1', 'overview?tab=drupal-to-okta-configuration')->save();

    $form['mo_con_okta_header_style'] = [
      '#markup' => t('<div class="mo_okta_header_container_step1">'),
    ];

    $form['header_setup1_guide_button'] = [
      '#markup' => '<div class="container-inline"><a class="button button--primary mo-guides-floating" target="_blank" href="https://plugins.miniorange.com/drupal-okta-user-sync">🕮 Setup guide</a></div> ',
    ];

    // <a class="button button--primary mo-guides-floating" target="_blank" href="https://plugins.miniorange.com/drupal-okta-user-sync">⏩ Setup Video</a>
    $form['header_top_style2'] = [
      '#markup' => '<h2><u>STEP 1/3</u></h2><br><h3>Okta App Configuration</h3><hr><br><div class="mo_okta_highlight_background"><p>Configure the following settings to register your Okta application in Drupal.</p></div>',

    ];

    $this->oktaConfigurationTable($form, $form_state, 'configuration');

    $form['configure_okta_back_button'] = [
      '#type' => 'submit',
      '#value' => t('&#11164; Back'),
      '#button_type' => 'danger',
      '#limit_validation_errors' => [],
      '#submit' => ["::backFun"],
      '#prefix' => '<div class="mo-table-button-center">',
    ];

    $form['configure_okta_test_button1'] = [
      '#type' => 'submit',
      '#value' => t('Save and Test Configuration'),
      '#button_type' => 'primary',
      '#submit' => ['::saveAndTestConfigureOkta'],

    ];

    if ($this->config->get('mo_okta_attr_list_from_server') != '') {
      $okta_test_data = Json::decode($this->config->get('mo_okta_attr_list_from_server'));
    }

    if (isset($okta_test_data['id'])) {
      $form['configure_okta_manual_sync_test'] = [
        '#type' => 'link',
        '#title' => t('Test Manual Sync'),
        '#url' => Url::fromRoute('okta_user_sync.testManualSync'),
        '#attributes' => ['class' => ['button', 'button--primary', 'use-ajax', 'js-form-submit', 'form-submit'] , 'data-dialog-type' => 'modal', 'data-dialog-options' => json_encode(['width' => '65%'])],
      ];
    }

    if (isset($okta_test_data['id']) || isset($okta_test_data['errorCode'])) {

      $form['configure_okta_next_button'] = [
        '#type' => 'submit',
        '#value' => t("Next &#11166;"),
        '#button_type' => 'primary',
        '#attributes' => ['style' => 'float:right'],
        '#disabled' => isset($okta_test_data['errorCode']),
        '#submit' => ["::nextBtnFun"],
        '#suffix' => '</div>',
      ];

      $this->testConfigurationResult($form, $form_state);
    }
    return $form;
  }

  /**
   * Shows test configuration result.
   *
   * @return array
   *   Returns array of form elements.
   */
  public function testConfigurationResult(array &$form, FormStateInterface $form_state): array {
    $okta_test_data = Json::decode($this->config->get('mo_okta_attr_list_from_server'));

    $form['okta_configuration_display_test_data'] = [
      '#type' => 'details',
      '#title' => $this->t('Test Configuration Result'),
      '#open' => isset($okta_test_data["errorCode"]) ,
      '#prefix' => '<div id="test_config_result">',
    ];

    $form['okta_configuration_display_test_data']['header_message'] = [
      '#markup' => isset($okta_test_data["errorCode"]) ?
      $this->t(
          '<div style="font-family:Calibre,serif;padding:0 3%;">
                <div style="color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 20px;text-align:center;border:1px solid #E6B3B2;font-size:18pt;">
                    ERROR
                </div><div style="color: #a94442;font-size:14pt; margin-bottom:20px;">'
      ) :
      $this->t(
          '<div style="font-family:Calibre,serif;padding:0 3%;"><div style="width:95%;color: #3c763d;background-color: #dff0d8;padding: 2%;margin-bottom: 20px;text-align: center;border: 1px solid #AEDB9A;font-size: 18pt;">
                    SUCCESS
                </div>'
      ),
    ];

    $form['okta_configuration_display_test_data']['okta_configuration_show_test_data'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attributes' => ['style' => 'border-collapse: separate;', 'class' => ['mo_okta_test_data']],
    ];

    if ($okta_test_data != NULL) {

      foreach ($okta_test_data as $key => $value) {

        $row = $this->oktaTestConfigurationData($key, $value);
        $form['okta_configuration_display_test_data']['okta_configuration_show_test_data'][$key] = $row;
      }
    }
    return $form;
  }

  /**
   * Shows test configuration result.
   *
   * @return array
   *   Returns array of form elements.
   */
  public function oktaTestConfigurationData($key, $value): array {
    $row[$key] = [
      '#type' => 'item',
      '#plain_text' => $key,
    ];

    $row[$value] = [
      '#type' => 'item',
      '#plain_text' => $value,
    ];
    return $row;
  }

  /**
   * For redirecting user to drupal to okta configuration page.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Returns response object
   */
  public function letsConfigureButton(array &$form, FormStateInterface $form_state) {
    $this->configFactory->set('okta_user_sync_status', 'overview?tab=drupal-to-okta-configuration')->save();
    $response = new RedirectResponse($this->base_url . '/admin/config/people/okta_user_sync/overview?tab=drupal-to-okta-configuration');
    $response->send();
    return new Response();
  }

  /**
   * {@inheritDoc}
   */
  public function saveAndTestConfigureOkta(array $form, FormStateInterface $form_state) {
    $form_values = ($form_state->getValues())['mo_configure_okta_configuration_table'];
    $data = [
      'Okta Portal URL:' => 'okta_user_sync_base_url',
      'Okta Bearer Token:' => 'okta_user_sync_bearer_token',
      'Test Your Configuration<br>Enter user email:' => 'okta_user_sync_upn',
    ];

    foreach ($data as $key => $value) {
      $this->configFactory->set($value, $form_values[$key][$value])->save();
    }

    $bearer_token = $this->configFactory->get('okta_user_sync_bearer_token');
    $provider_base_url = $this->configFactory->get('okta_user_sync_base_url');
    \Drupal::configFactory()->getEditable('user_provisioning.settings')->set('mo_user_provisioning_configured_application', 'okta')
      ->set('mo_provider_specific_provisioning_base_url', $provider_base_url)
      ->set('mo_provider_specific_provisioning_api_token', $bearer_token)
      ->save();

    $okta_helper = new MoOktaHelper();
    $okta_helper->fetchAttributes();

    $okta_test_data = Json::decode($this->config->get('mo_okta_attr_list_from_server'));
    if ($okta_test_data != '') {
      if (isset($okta_test_data["errorCode"])) {
        $this->messenger->addError(t('An error occurred while performing test configuration. Please refer to <a href="#test_config_result">Test Configuration Result</a> for more information.'));
      }
      else {
        $this->messenger->addstatus(t('Test Configuration successful. You can check the complete list of attributes received from Okta by clicking on the link <a href="#test_config_result">HERE</a>.'));
      }
    }
  }

  /**
   * For returning to drupal to okta configuration page.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Returns response object
   */
  public function backBtnHowToSync(array &$form, FormStateInterface $form_state) {
    $this->configFactory->set('okta_user_sync_status', 'drupal-to-okta-configuration')->save();
    $response = new RedirectResponse($this->base_url . '/admin/config/people/okta_user_sync/overview?tab=drupal-to-okta-configuration');
    $response->send();
    return new Response();
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
