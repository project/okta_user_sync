<?php

namespace Drupal\okta_user_sync\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\user_provisioning\Helpers\moUserProvisioningLogger;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\user_provisioning\moUserProvisioningConstants;
use Drupal\user_provisioning\moUserProvisioningEntityFactory;
use Drupal\user_provisioning\moUserProvisioningOperationsHandler;

/**
 * Class for handling diffrent provisioning operations.
 */
class MoOktaProvisioningConfiguration extends FormBase {

  /**
   * Base URL of the site.
   *
   * @var string
   */
  private $base_url;

  /**
   * ImmutableConfig property.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Config property.
   *
   * @var Drupal\Core\Config\Config
   */
  protected $configFactory;

  /**
   * Messenger property.
   *
   * @var object
   */
  protected $messenger;

  /**
   * URL Path property.
   *
   * @var string
   */
  private $urlPath;

  /**
   * Logger property.
   *
   * @var Drupal\user_provisioning\Helpers\moUserProvisioningLogger
   */
  private moUserProvisioningLogger $moLogger;

  /**
   * Constructs a new MoOktaProvisioningConfiguration object.
   */
  public function __construct() {
    global $base_url;
    $this->base_url = $base_url;
    $this->config = \Drupal::config('okta_user_sync.settings');
    $this->configFactory = \Drupal::configFactory()->getEditable('okta_user_sync.settings');
    $this->messenger = \Drupal::messenger();
    $this->urlPath = $base_url . '/' . \Drupal::service('extension.list.module')->getPath('okta_user_sync') . '/includes';
    $this->moLogger = new moUserProvisioningLogger();
    $this->disabled_fieldset = FALSE;
  }

  /**
   * FormID of the form.
   *
   * @return string
   *   Returns formID of the form.
   */
  public function getFormId() {
    return 'MoOktaProvisioningConfiguration';
  }

  /**
   * Builds provisining configuration form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#prefix'] = '<div id="okta_provisioning_configuration">';
    $form['#suffix'] = '</div>';
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['markup_library'] = [
      '#attached' => [
        'library' => [
          "okta_user_sync/okta_user_sync.admin",
          'okta_user_sync/okta_user_sync.okta_test',
          'core/drupal.dialog.ajax',
        ],
      ],
    ];

    $provisioning_type_url = $_SERVER['REQUEST_URI'];

    if (strpos($provisioning_type_url, 'manualConfiguration') !== FALSE) {
      $this->manualConfiguration($form, $form_state);
    }
    elseif (strpos($provisioning_type_url, 'automaticConfiguration') !== FALSE) {
      $this->automaticConfiguration($form, $form_state);
    }
    elseif (strpos($provisioning_type_url, 'attributeListOfOkta') !== FALSE) {
      $this->attributeListOfOkta($form, $form_state);
    }
    elseif (strpos($provisioning_type_url, 'testManualSync') !== FALSE || strpos($_SERVER['REQUEST_URI'], 'syncUserManually') !== FALSE) {
      $this->testManualSync($form, $form_state);
    }

    return $form;
  }

  /**
   * Test manual user sync.
   *
   * @return array
   *   Returns array of form elements.
   */
  public function testManualSync(array &$form, FormStateInterface $form_state) {

    $message = $_COOKIE['checkBoxMessage'];
    if (isset($message) && strlen($message) > 0) {
      if ($message == "both unchecked") {
        $this->configFactory
          ->set('okta_user_sync_enable_manual_provisioning_checkbox', FALSE)
          ->set('okta_user_sync_deactivate_user_manual_provisioning_checkbox', FALSE)
          ->save();
        $this->messenger->addError("Please enable the Create or Deactivate user checkbox to sync the user at configured application");

      }
      elseif ($message == "Deactivate") {
        $this->configFactory
          ->set('okta_user_sync_deactivate_user_manual_provisioning_checkbox', FALSE)
          ->set('okta_user_sync_enable_manual_provisioning_checkbox', TRUE)
          ->save();
      }
      elseif ($message == "Create") {
        $this->configFactory
          ->set('okta_user_sync_enable_manual_provisioning_checkbox', FALSE)
          ->set('okta_user_sync_deactivate_user_manual_provisioning_checkbox', TRUE)
          ->save();
      }
      else {
        $this->configFactory
          ->set('okta_user_sync_enable_manual_provisioning_checkbox', TRUE)
          ->set('okta_user_sync_deactivate_user_manual_provisioning_checkbox', TRUE)
          ->save();
      }
    }

    if ($message != "both unchecked") {

      $form['manual_provisioning_test']['create_user_fieldset'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Sync user manually<hr>'),
      ];

      $form['manual_provisioning_test']['create_user_fieldset']['mo_okta_drupal_username'] = [
        '#type' => 'textfield',
        '#attributes' => ['placeholder' => 'Search Drupal username of user to sync'],
        '#autocomplete_route_name' => 'user_provisioning.autocomplete',
        '#prefix' => '<p class="mo_okta_highlight_background"><strong>Note:</strong> Search the username of user to sync (create/deactivate) it to the Okta. (After user creation, the user will be created in a <strong>Staged</strong> state. The admin has to activate it manually. If you want to create a user in an activated state, please send us an email on <a href="drupalsupport@xecurify.com">drupalsupport@xecurify.com</a> .)</p><div class="container-inline">',
      ];

      $form['manual_provisioning_test']['create_user_fieldset']['mo_okta_sync_button'] = [
        '#type' => 'submit',
        '#value' => t('Sync'),
        '#button_type' => 'primary',
        '#attributes' => [
          'class' => [
            'use-ajax',
            'button--primary',
            'mo_okta_sync_button',
          ],

        ],
        '#ajax' => [
          'callback' => [$this, 'moDrupalToOktaSyncTest'],
          'event' => 'click',
        ],
      ];

      if (strpos($_SERVER['REQUEST_URI'], 'syncUserManually') !== FALSE) {
        $form['manual_provisioning_test']['create_user_fieldset']['sync_all_button'] = [
          '#type' => 'submit',
          '#value' => t('Sync All Users'),
          '#button_type' => 'primary',
          '#attributes' => ['class' => ['mo_okta_sync_all_button']],
          '#disabled' => TRUE,
        ];
      }
    }
    return $form;
  }

  /**
   * Shows automatic configurations.
   *
   * @return array
   *   Returns array of form elements.
   */
  public function automaticConfiguration(array &$form, FormStateInterface $form_state): array {
    $form['okta_provisioning_operations'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attributes' => ['class' => ['mo_okta_provisioning_operations_table']],
      '#prefix' => '<br><div class="mo_okta_background_note">Choose operations in Automatic Provisioning</div>',
    ];

    $row = $this->moProvisioningOperations('automatic');
    $form['okta_provisioning_operations']['operations'] = $row;

    $form['create_user_types'] = [
      '#type' => 'fieldset',
      '#attributes' => ['style' => 'box-shadow: none'],
      '#title' => t('Select the user creation option<hr>'),
      '#states' => [
        'visible' => [
          ':input[name="okta_provisioning_operations[operations][create_user_okta]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $options = [t('Create User without Credentials') , t('Create User with Password')];
    $form['create_user_types']['create_user_radio_buttons'] = [
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => $this->configFactory->get('okta_user_sync_create_user_auto_radio_button'),
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['send_are'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button--primary',
        ],
      ],
      '#ajax' => [
        'callback' => '::saveSettingsOktaAutomaticProv',
        'event' => 'click',
      ],
    ];
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $form;
  }

  /**
   * Shows manual configurations.
   *
   * @return array
   *   Returns array of form elements.
   */
  public function manualConfiguration(array &$form, FormStateInterface $form_state): array {
    $mo_premium_tag = '<a href = "okta_upgrade_plans" >[PREMIUM]</a>';

    $form['okta_provisioning_operations'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attributes' => ['class' => ['mo_okta_provisioning_operations_table']],
      '#prefix' => '<br><div class="mo_okta_background_note">Choose operations in Manual Provisioning</div>',
    ];
    $row = $this->moProvisioningOperations('manual');
    $form['okta_provisioning_operations']['operations'] = $row;

    $form['create_user_types'] = [
      '#type' => 'fieldset',
      '#attributes' => ['style' => 'box-shadow: none'],
      '#title' => $this->t('<div>Select the user creation option <a href="okta_upgrade_plans"><img class="scim_client_know_more1" src="' . $this->urlPath . '/crown.png" alt="Premium"><span class="scim_client_know_more_hidden">Available in the Premium version</span></a></div><hr>'),

    ];

    $form['create_user_types']['create_user_radio_button'] = [
      '#type' => 'radios',
      '#default_value' => 0,
      '#options' => [t('Create User without Credentials'), t('Create Activated User without Credentials'), t('Create User with Recovery Question'), t('Create Activated User with Recovery Question'), t('Create User in Group')],
      '#disabled' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['send_are'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button--primary',
        ],
      ],
      '#ajax' => [
        'callback' => '::saveSettingsOktaManualProv',
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $form;
  }

  /**
   * Shows provisioning configuration methods.
   *
   * @return array
   *   Returns array of provisioning methods form elements.
   */
  private function moProvisioningOperations($provision_type): array {
    $mo_premium_tag = '<a href = "okta_upgrade_plans" >[PREMIUM]</a>';
    $row['read_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => t('Read user'),
      '#default_value' => $provision_type != 'scheduler',
      '#prefix' => '<div class="container-inline">',
    ];

    $row['create_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => t('Create user '),
      '#default_value' => $provision_type == 'scheduler' ? FALSE : $this->config->get('okta_user_sync_enable_' . $provision_type . '_provisioning_checkbox'),
      '#disabled' => $provision_type == 'scheduler',
    ];

    $row['deactivate_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => t('Deactivate user'),
      '#disabled' => $provision_type == 'scheduler',
      '#default_value' => $provision_type == 'scheduler' ? FALSE : $this->config->get('okta_user_sync_deactivate_user_' . $provision_type . '_provisioning_checkbox'),

    ];

    $row['update_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => $provision_type == 'scheduler' ? t('Update user') : t('Update user ') . $mo_premium_tag,
      '#disabled' => TRUE,
    ];

    $row['delete_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => $provision_type == 'scheduler' ? t('Delete user') : t('Delete user ') . $mo_premium_tag,
      '#disabled' => TRUE,
    ];

    return $row;
  }

  /**
   * Saves manual provisioning configuration.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns ajaxresponse object.
   */
  public function saveSettingsOktaManualProv(array &$form, FormStateInterface $form_state) {

    $this->configFactory
      ->set('okta_user_sync_enable_manual_provisioning_checkbox', $form_state->getValues()['okta_provisioning_operations']['operations']['create_user_okta'])
      ->set('okta_user_sync_create_user_manual_radio_button', '0')
      ->set('okta_user_sync_deactivate_user_manual_provisioning_checkbox', $form_state->getValues()['okta_provisioning_operations']['operations']['deactivate_user_okta'])
      ->save();

    $this->messenger->addStatus(t('Configuration saved successfully.'));

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#okta_provisioning_configuration', $form));
    return $response;
  }

  /**
   * Saves automatic provisioning configuration.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns ajaxresponse object.
   */
  public function saveSettingsOktaAutomaticProv(array &$form, FormStateInterface $form_state) {

    $automatic_create_user = $form_state->getValues()['okta_provisioning_operations']['operations']['create_user_okta'];

    $this->configFactory
      ->set('okta_user_sync_enable_automatic_provisioning_checkbox', $automatic_create_user)
      ->set('okta_user_sync_create_user_auto_radio_button', $form_state->getValues()['create_user_radio_buttons'])
      ->set('okta_user_sync_deactivate_user_automatic_provisioning_checkbox', $form_state->getValues()['okta_provisioning_operations']['operations']['deactivate_user_okta'])
      ->save();

    $this->messenger->addstatus(t('Configurations saved successfully.'));
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#okta_provisioning_configuration', $form));
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Performs deactivation of user.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns ajaxresponse object.
   */
  public function moDrupalToOktaSyncTest(array &$form, FormStateInterface $form_state) {

    $this->configFactory->set('okta_user_sync_request_to_manual_sync', TRUE)->save();
    $sync_username = ($form_state->getValues())['mo_okta_drupal_username'];
    $this->moLogger->addLog('Provisioning on demand. Entered entity name is :' . $sync_username, __LINE__, __FUNCTION__, basename(__FILE__));

    $entity = $this->getEntityToProvision($sync_username);

    if (is_null($entity)) {
      $this->messenger->addError('"' . $sync_username . '"' . $this->t(' can not be recognized as a valid User or Role. Enter a valid entity( User or Role) to provision.'));
      $response = new AjaxResponse();
      $response->addCommand(new ReplaceCommand('#okta_provisioning_configuration', $form));
      return $response;
    }
    else {
      $mo_entity_handler = new moUserProvisioningOperationsHandler($entity);
      $operationObject = moUserProvisioningEntityFactory::getEntityHandler($entity);
      try {
        [$status_code, $content, $conflict] = $operationObject->searchResource();
        if ($entity->isBlocked() && $this->configFactory->get('okta_user_sync_deactivate_user_manual_provisioning_checkbox')) {
          if ($conflict == moUserProvisioningConstants::OKTA_CONFLICT) {
            $result = $mo_entity_handler->deactivate();

            if ($result == '{}') {
              $this->messenger->addMessage($this->t($entity->label() . ' successfully deactivated at the configured application.'));
              $this->configFactory->set('okta_user_sync_deactivate_user_manually', 'successful')->save();
            }
            else {
              $this->messenger->addError($this->t('An error occurred while provisioning the user, please refer to <a href="' . $this->base_url . moUserProvisioningConstants::DRUPAL_LOGS_PATH . '">drupal logs</a> for more information.'));
              if ($this->configFactory->get('okta_user_sync_deactivate_user_manually') !== 'successful') {
                $this->configFactory->set('okta_user_sync_deactivate_user_manually', 'unsuccessful')->save();
              }
            }
            $response = new AjaxResponse();
            $response->addCommand(new ReplaceCommand('#okta_provisioning_configuration', $form));
            return $response;

          }
          else {
            \Drupal::logger('user_provisioning')->error(__FUNCTION__ . ': ' . t('For Deactivation User is not found in Configured Application.'));
            $this->messenger->addError($this->t('For Deactivation User is not found in Configured Application.'));
            if ($this->configFactory->get('okta_user_sync_deactivate_user_manually') !== 'successful') {
              $this->configFactory->set('okta_user_sync_deactivate_user_manually', 'unsuccessful (User not found)')->save();
            }
          }
          $response = new AjaxResponse();
          $response->addCommand(new ReplaceCommand('#okta_provisioning_configuration', $form));
          return $response;
        }
        elseif ($this->configFactory->get('okta_user_sync_enable_manual_provisioning_checkbox')) {
          try {
            $result = $mo_entity_handler->insert();
            if (is_null($result)) {
              $this->messenger->addError($this->t('An error occurred while provisioning the user, please refer to <a href="' . $this->base_url . moUserProvisioningConstants::DRUPAL_LOGS_PATH . '">drupal logs</a> for more information.'));

              $response = new AjaxResponse();
              $response->addCommand(new ReplaceCommand('#okta_provisioning_configuration', $form));
              return $response;
            }
            $this->messenger->addMessage($this->t($entity->label() . ' successfully created at the configured application.'));
            $response = new AjaxResponse();
            $response->addCommand(new ReplaceCommand('#okta_provisioning_configuration', $form));
            return $response;
          }
          catch (\Exception $exception) {
            $this->messenger->addError($exception->getMessage());
          }
        }
        else {
          $response = new AjaxResponse();
          $response->addCommand(new ReplaceCommand('#okta_provisioning_configuration', $form));
          return $response;
        }
      }
      catch (\Exception $exception) {
        $this->messenger->addError($exception->getMessage());
      }
    }
  }

  /**
   * Provisions a specific user.
   *
   * @return Drupal\user\Entity\User
   *   Returns user entity if it exists else false
   */
  private function getEntityToProvision(string $entity_name) {
    $user = user_load_by_name($entity_name);
    if ($user != FALSE) {
      return User::load($user->id());
    }
    return NULL;
  }

  /**
   * Shows attribute list received from okta.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function attributeListOfOkta(array &$form, FormStateInterface $form_state) {
    $okta_test_data = Json::decode($this->config->get('mo_okta_attr_list_from_server'));

    $form['okta_configuration_display_test_data'] = [
      '#type' => 'fieldset',
      '#prefix' => '<div id="test_config_result">',
    ];

    $form['okta_configuration_display_test_data']['header_message'] = [
      '#markup' => isset($okta_test_data["errorCode"]) ?
      $this->t(
          '<div style="font-family:Calibre,serif;padding:0 3%;">
                <div style="color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 20px;text-align:center;border:1px solid #E6B3B2;font-size:18pt;">
                    ERROR
                </div><div style="color: #a94442;font-size:14pt; margin-bottom:20px;">'
      ) :
      $this->t(
          '<div style="font-family:Calibre,serif;padding:0 3%;"><div style="width:95%;color: #3c763d;background-color: #dff0d8;padding: 2%;margin-bottom: 20px;text-align: center;border: 1px solid #AEDB9A;font-size: 18pt;">
                    SUCCESS
                </div>'
      ),
    ];

    $form['okta_configuration_display_test_data']['okta_configuration_show_test_data'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attributes' => ['style' => 'border-collapse: separate;', 'class' => ['mo_okta_test_data']],
    ];

    if ($okta_test_data != NULL) {

      foreach ($okta_test_data as $key => $value) {
        $test_config_table = new MoOktaOverview();
        $row = $test_config_table->oktaTestConfigurationData($key, $value);
        $form['okta_configuration_display_test_data']['okta_configuration_show_test_data'][$key] = $row;
      }
    }

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['send_are'] = [
      '#type' => 'submit',
      '#value' => $this->t('Close'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button--primary',
        ],
      ],
      '#ajax' => [
        'callback' => '::closeAttributeListPopup',
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * Closes attr list form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns ajaxresponse object.
   */
  public function closeAttributeListPopup(array &$form, FormStateInterface $form_state) {
    $command = new CloseModalDialogCommand('okta_provisioning_configuration');
    $response = new AjaxResponse();
    $response->addCommand($command);
    return $response;
  }

}
