<?php

namespace Drupal\okta_user_sync\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\okta_user_sync\Helper\MoOktaHelper;
use Drupal\user_provisioning\Helpers\moUserProvisioningLogger;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class for configuring the Drupal to Okta provisioning.
 */
class MoDrupalToOkta extends FormBase {

  /**
   * Base URL of the site.
   *
   * @var string
   */
  private $base_url;

  /**
   * ImmutableConfig property.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Config property.
   *
   * @var Drupal\Core\Config\Config
   */
  protected $configFactory;

  /**
   * Messenger property.
   *
   * @var object
   */
  protected $messenger;

  /**
   * URL Path property.
   *
   * @var string
   */
  private $urlPath;

  /**
   * Logger property.
   *
   * @var Drupal\user_provisioning\Helpers\moUserProvisioningLogger
   */
  private moUserProvisioningLogger $moLogger;

  /**
   * Constructs a new MoDrupalToOkta object.
   */
  public function __construct() {
    global $base_url;
    $this->base_url = $base_url;
    $this->config = \Drupal::config('okta_user_sync.settings');
    $this->configFactory = \Drupal::configFactory()->getEditable('okta_user_sync.settings');
    $this->messenger = \Drupal::messenger();
    $this->moLogger = new moUserProvisioningLogger();
    $this->urlPath = $base_url . '/' . \Drupal::service('extension.list.module')->getPath('okta_user_sync') . '/includes';
  }

  /**
   * FormID of the form.
   *
   * @return string
   *   Returns formID of the form.
   */
  public function getFormId() {
    return 'MoDrupalToOkta';
  }

  /**
   * Builds okta user provisioning form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    \Drupal::configFactory()->getEditable('okta_user_sync.settings')->set('okta_user_sync_status1', 'mo_drupal_to_okta')->save();

    $this->config = \Drupal::config('okta_user_sync.settings');
    $this->configFactory = \Drupal::configFactory()->getEditable('okta_user_sync.settings');
    $form['mo_okta_sync_add_css'] = [
      '#attached' => [
        'library' => [
          'okta_user_sync/okta_user_sync.admin',
          'okta_user_sync/okta_user_sync.okta_test',
          'core/drupal.dialog.ajax',
        ],
      ],
    ];

    $okta_test_data = '';
    if (($this->config->get('mo_okta_attr_list_from_server')) != '') {
      $okta_test_data = Json::decode($this->config->get('mo_okta_attr_list_from_server'));
    }
    $disabled = FALSE;
    if (isset($okta_test_data['errorCode'])) {
      $disabled = TRUE;
    }
    if ($disabled) {
      \Drupal::messenger()->addError('An error occurred while performing the test configuration. Please click on the Review Okta Configurations button to perform the Test configuration again. Test Configuration must be successful to perform user sync from Drupal to Okta.');

    }
    $form['mo_summary_okta_header_style'] = [
      '#markup' => t('<div class="mo_okta_header_container_summary">'),
    ];

    $form['drupal_to_okta_review_configuration_button'] = [
      '#type' => 'link',
      '#title' => t('Review Okta Configuration'),
      '#url' => Url::fromRoute('okta_user_sync.reviewOktaConfiguration'),
      '#attributes' => ['style' => ['float:right'], 'class' => ['button', 'button--primary', 'use-ajax', 'js-form-submit', 'form-submit'] , 'data-dialog-type' => 'modal', 'data-dialog-options' => json_encode(['width' => '65%'])],
    ];

    if (!$disabled) {
      $form['drupal_to_okta_sync_manually_button'] = [
        '#type' => 'link',
        '#title' => t('Sync User Manually'),
        '#url' => Url::fromRoute('okta_user_sync.syncUserManually'),
        '#attributes' => ['style' => ['float:right'], 'class' => ['button', 'button--primary', 'use-ajax', 'js-form-submit', 'form-submit'], 'data-dialog-type' => 'modal', 'data-dialog-options' => json_encode(['width' => '65%'])],
      ];
    }

    $this->manualConfiguration($form, $form_state);
    $this->automaticConfiguration($form, $form_state);
    $this->schedulerConfiguration($form, $form_state);

    $form['drupal_to_okta_save_button'] = [
      '#type' => 'submit',
      '#id' => 'save_drupal_to_okta_settings',
      '#value' => t('Save Settings'),
      '#button_type' => 'primary',
      '#submit' => ["::saveAllSettings"],
    ];

    $okta_handler = new MoOktaHelper();
    $okta_handler->moOktaShowCustomerSupportIcon($form, $form_state);

    return $form;
  }

  /**
   * Provisioning operations for .
   *
   * @return array
   *   Returns array of form elements.
   */
  private function moProvisioningOperations($provision_type): array {
    $mo_premium_tag = '<a href = "okta_upgrade_plans" >[PREMIUM]</a>';

    $okta_test_data = '';
    if (($this->config->get('mo_okta_attr_list_from_server')) != '') {
      $okta_test_data = Json::decode($this->config->get('mo_okta_attr_list_from_server'));
    }
    $disabled = FALSE;
    if (isset($okta_test_data['errorCode'])) {
      $disabled = TRUE;
    }

    $row['read_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => t('Read user '),
      '#default_value' => $provision_type != 'scheduler',
      '#disabled' => $provision_type == 'scheduler' || $disabled,
      '#prefix' => '<div class="container-inline">',
    ];

    $row['create_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => t('Create user '),
      '#default_value' => $provision_type == 'scheduler' ? FALSE : $this->config->get('okta_user_sync_enable_' . $provision_type . '_provisioning_checkbox'),
      '#disabled' => $provision_type == 'scheduler' || $disabled ,
    ];

    $row['deactivate_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => $provision_type == 'scheduler' ? t('Deactivate user ') : t('Deactivate user '),
      '#disabled' => $provision_type == 'scheduler' || $disabled,
      '#default_value' => $provision_type == 'scheduler' ? FALSE : $this->config->get('okta_user_sync_deactivate_user_' . $provision_type . '_provisioning_checkbox'),
    ];

    $row['update_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => $provision_type == 'scheduler' ? t('Update user ') : t('Update user ') . $mo_premium_tag,
      '#disabled' => TRUE,
    ];

    $row['delete_user_okta'] = [
      '#type' => 'checkbox',
      '#title' => $provision_type == 'scheduler' ? t('Delete user ') : t('Delete user ') . $mo_premium_tag,
      '#disabled' => TRUE,
    ];

    return $row;
  }

  /**
   * Configuration for manual provisioning.
   *
   * @return array
   *   Returns array of form elements.
   */
  public function manualConfiguration(array &$form, FormStateInterface $form_state): array {
    $form['mo_okta_manual_provisioning_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Manual Provisioning Configuration<hr>'),
      '#disabled' => TRUE,
      '#prefix' => '<br><br><br>',
    ];

    $form['mo_okta_manual_provisioning_fieldset']['okta_manual_provisioning_operations'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attributes' => ['style' => 'border-collapse: separate;'],
    ];

    $row = $this->moProvisioningOperations('manual');
    $form['mo_okta_manual_provisioning_fieldset']['okta_manual_provisioning_operations']['operations'] = $row;

    $this->showManualSyncMethods($form, $form_state);

    return $form;
  }

  /**
   * Configuration for automatic provisioning.
   *
   * @return array
   *   Returns array of form elements.
   */
  public function automaticConfiguration(array &$form, FormStateInterface $form_state): array {
    $form['mo_okta_automatic_provisioning_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Automatic Provisioning Configuration<hr>'),
    ];

    $form['mo_okta_automatic_provisioning_fieldset']['okta_automatic_provisioning_operations'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attributes' => ['style' => 'border-collapse: separate;'],
    ];

    $row = $this->moProvisioningOperations('automatic');
    $form['mo_okta_automatic_provisioning_fieldset']['okta_automatic_provisioning_operations']['operations'] = $row;

    $this->showAutomaticSyncMethods($form, $form_state);

    return $form;
  }

  /**
   * For returning to configure provisining page.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Returns response object
   */
  public function backFunProvTypes(array &$form, FormStateInterface $form_state) {
    $status = $this->config->get('okta_user_sync_status');
    if ($status == 'overview') {
      $response = new RedirectResponse($this->base_url . '/admin/config/people/okta_user_sync/overview');
    }
    else {
      $response = new RedirectResponse($this->base_url . '/admin/config/people/okta_user_sync/overview?tab=' . $status);
    }

    $response->send();
    return new Response();

  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement submitForm() method.
  }

  /**
   * Shows manual provisioning methods.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function showManualSyncMethods(array &$form, FormStateInterface $form_state) {
    $mo_premium_tag = '<a href = "okta_upgrade_plans" >[PREMIUM]</a>';
    $form['mo_okta_manual_provisioning_fieldset']['create_user_types'] = [
      '#type' => 'fieldset',
      '#attributes' => ['style' => 'box-shadow: none'],
      '#title' => $this->t('Select the user creation option ' . $mo_premium_tag . '<hr>'),
      '#states' => [
        'visible' => [
          ':input[name="okta_manual_provisioning_operations[operations][create_user_okta]"]' => ['checked' => TRUE],
        ],
      ],

    ];

    $form['mo_okta_manual_provisioning_fieldset']['create_user_types']['create_user_radio_button'] = [
      '#type' => 'radios',
      '#default_value' => 0,
      '#options' => [t('Create User without Credentials'), t('Create Activated User without Credentials'), t('Create User with Recovery Question'), t('Create Activated User with Recovery Question'), t('Create User in Group')],
      '#disabled' => TRUE,
    ];
    return $form;
  }

  /**
   * Shows automatic provisioning methods.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function showAutomaticSyncMethods(array &$form, FormStateInterface $form_state) {
    $okta_test_data = '';
    if (($this->config->get('mo_okta_attr_list_from_server')) != '') {
      $okta_test_data = Json::decode($this->config->get('mo_okta_attr_list_from_server'));
    }
    $disabled = FALSE;
    if (isset($okta_test_data['errorCode'])) {
      $disabled = TRUE;
    }

    $mo_premium_tag = '<a href = "okta_upgrade_plans" >[PREMIUM]</a>';
    $form['mo_okta_automatic_provisioning_fieldset']['create_user_types'] = [
      '#type' => 'fieldset',
      '#attributes' => ['style' => 'box-shadow: none'],
      '#title' => t('Select the user creation option<hr>'),
      '#states' => [
        'visible' => [
          ':input[name="okta_automatic_provisioning_operations[operations][create_user_okta]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $options = [t('Create User without Credentials') , t('Create User with Password')];
    $form['mo_okta_automatic_provisioning_fieldset']['create_user_types']['create_user_radio_buttons'] = [
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => $this->configFactory->get('okta_user_sync_create_user_auto_radio_button'),
      '#disabled' => $disabled,

    ];

    $form['mo_okta_automatic_provisioning_fieldset']['create_user_types']['premium_heading'] = [
      '#markup' => '<br><b>Other Premium methods ' . $mo_premium_tag . '</b>',
    ];
    $form['mo_okta_automatic_provisioning_fieldset']['create_user_types']['create_user_radio_button'] = [
      '#type' => 'radios',
      '#options' => [0 => t('Create Activated User without Credentials'), 1 => t('Create Activated User with Password'), 2 => t('Create User with Recovery Question'), 3 => t('Create Activated User with Recovery Question'), 4 => t('Create User in Group'), 5 => t('Create User with Password & Recovery Question'), 6 => t('Create Activated User with Password & Recovery Question')],
      '#disabled' => TRUE,
    ];

  }

  /**
   * Shows scheduler provisioning methods.
   *
   * @return array
   *   Returns array of form elements.
   */
  private function schedulerConfiguration(array &$form, FormStateInterface $form_state) {
    $mo_premium_tag = '<a href = "okta_upgrade_plans" >[PREMIUM]</a>';

    $form['mo_okta_scheduler_provisioning_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Scheduler Based  Provisioning Configuration ' . $mo_premium_tag . '<hr>'),
    ];

    $form['mo_okta_scheduler_provisioning_fieldset']['okta_scheduler_provisioning_operations'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#attributes' => ['style' => 'border-collapse: separate;'],
    ];

    $row = $this->moProvisioningOperations('scheduler');
    $form['mo_okta_scheduler_provisioning_fieldset']['okta_scheduler_provisioning_operations']['operations'] = $row;

    $this->showschedulerConfiguration($form, $form_state);

    return $form;
  }

  /**
   * Shows scheduler provisioning methods.
   *
   * @return array
   *   Returns array of form elements.
   */
  public function showschedulerConfiguration(array &$form, FormStateInterface $form_state): array {

    $form['mo_okta_scheduler_provisioning_fieldset']['mo_okta_scheduler_provisioning_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configuration ') . '<hr>',
      '#attributes' => ['style' => 'box-shadow: none'],
    ];

    $form['mo_okta_scheduler_provisioning_fieldset']['mo_okta_scheduler_provisioning_fieldset']['auto_creation'] = [
      '#type' => 'number',
      '#title' => t('Fetch All users details in next * minutes'),
      '#attributes' => ['style' => 'width: 45%'],
      '#disabled' => TRUE,

    ];
    return $form;
  }

  /**
   * Saves provisioning configurations.
   */
  public function saveAllSettings(array $form, FormStateInterface $form_state) {
    $create_user_automatically = $form_state->getValues()['okta_automatic_provisioning_operations']['operations']['create_user_okta'];
    $default_automatic_provisioning = ($create_user_automatically != 0) ? $form_state->getValues()['create_user_radio_buttons'] : 0;

    $this->configFactory
      ->set('okta_user_sync_enable_manual_provisioning_checkbox', $form_state->getValues()['okta_manual_provisioning_operations']['operations']['create_user_okta'])
      ->set('okta_user_sync_enable_automatic_provisioning_checkbox', $create_user_automatically)
      ->set('okta_user_sync_create_user_auto_radio_button', $default_automatic_provisioning)
      ->set('okta_user_sync_create_user_manual_radio_button', '0')
      ->set('okta_user_sync_deactivate_user_automatic_provisioning_checkbox', $form_state->getValues()['okta_automatic_provisioning_operations']['operations']['deactivate_user_okta'])
      ->set('okta_user_sync_deactivate_user_manual_provisioning_checkbox', $form_state->getValues()['okta_manual_provisioning_operations']['operations']['deactivate_user_okta'])
      ->save();

    $this->messenger->addMessage('Configuration saved successfully.');
  }

}
