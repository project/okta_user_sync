<?php

namespace Drupal\user_provisioning\ProviderSpecific\ResponseProcessor\UserResponseProcessor;

use Drupal\user_provisioning\moUserProvisioningConstants;
use Drupal\user_provisioning\ProviderSpecific\ResponseProcessor\moResourceResponseProcessorInterface;
use Psr\Http\Message\ResponseInterface;

/**
 *
 */
class moUserSCIMResponseProcessor implements moResourceResponseProcessorInterface {

  /**
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Response received after the API call.
   * @return array Status code and the content of the response
   */
  public function get(ResponseInterface $response): array {
    $status_code = $response->getStatusCode();
    $content = $response->getBody()->getContents();

    // Initializing as conflict is not determined.
    $conflict = moUserProvisioningConstants::SCIM_CONFLICT_UNDETERMINED;
    if ($status_code == 200) {
      $response_body = json_decode($content, TRUE);
      if (isset($response_body['totalResults'])) {
        if ($response_body['totalResults'] == 0) {

          // No conflict exists if no matching entity is found.
          $conflict = moUserProvisioningConstants::SCIM_NO_CONFLICT;
        }
        else {
          // Setting as conflict since one or more entity at the configured application is matched with the requested query.
          $content = moUserProvisioningConstants::SCIM_CONFLICT;
        }
      }
    }
    return [$status_code, $content, $conflict];
  }

  /**
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Response received after the API call.
   *
   * @return array Status code and the content of the response
   */
  public function post(ResponseInterface $response): array {
    $status_code = $response->getStatusCode();
    $content = $response->getBody()->getContents();

    // @todo need to add the content and its details at the database to refer for future api calls.
    return [$status_code, $content];
  }

  /**
   * @param \Psr\Http\Message\ResponseInterface $response
   *
   * @return void
   */
  public function patch(ResponseInterface $response) {
    // @todo Implement patch() method.
  }

  /**
   * @param \Psr\Http\Message\ResponseInterface $response
   *
   * @return void
   */
  public function put(ResponseInterface $response) {
    // @todo Implement put() method.
  }

  /**
   * @param \Psr\Http\Message\ResponseInterface $response
   *
   * @return void
   */
  public function delete(ResponseInterface $response) {
    // @todo Implement delete() method.
  }

  /**
   *
   */
  public function deactivate(ResponseInterface $response) {
    // @todo Implement deactivate() method.
  }

}
