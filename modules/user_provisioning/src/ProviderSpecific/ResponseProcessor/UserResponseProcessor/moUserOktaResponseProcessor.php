<?php

namespace Drupal\user_provisioning\ProviderSpecific\ResponseProcessor\UserResponseProcessor;

use Drupal\user_provisioning\moUserProvisioningConstants;
use Drupal\user_provisioning\ProviderSpecific\ResponseProcessor\moResourceResponseProcessorInterface;
use Psr\Http\Message\ResponseInterface;

/**
 *
 */
class moUserOktaResponseProcessor implements moResourceResponseProcessorInterface {

  /**
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Response received after the API call.
   * @return array Status code and the content of the response
   */
  public function get(ResponseInterface $response):array {
    $status_code = $response->getStatusCode();
    $content = $response->getBody()->getContents();

    // Initializing as conflict is not determined.
    $conflict = moUserProvisioningConstants::OKTA_CONFLICT_UNDETERMINED;
    if ($status_code == 200) {
      $response = json_decode($content, TRUE);
      $user_id = NULL;
      if (isset($response[0]['id'])) {
        $user_id = $response[0]['id'];
      }
      if (is_null($user_id)) {
        // No conflict exists if no matching entity is found.
        $conflict = moUserProvisioningConstants::OKTA_NO_CONFLICT;
      }
      else {
        // Setting as conflict since one or more entity at the configured application is matched with the requested query.
        $conflict = moUserProvisioningConstants::OKTA_CONFLICT;
      }
    }
    return [$status_code, $content, $conflict];
  }

  /**
   *
   */
  public function post(ResponseInterface $response) {
    $status_code = $response->getStatusCode();
    $content = $response->getBody()->getContents();

    // @todo need to add the content and its details at the database to refer for future api calls.
    return [$status_code, $content];
  }

  /**
   *
   */
  public function patch(ResponseInterface $response) {
    // @todo Implement patch() method.
  }

  /**
   *
   */
  public function put(ResponseInterface $response) {
    // @todo Implement put() method.
  }

  /**
   *
   */
  public function delete(ResponseInterface $response) {
    // @todo Implement delete() method.
  }

  /**
   *
   */
  public function deactivate(ResponseInterface $response) {
    $status_code = $response->getStatusCode();
    $content = $response->getBody()->getContents();
    return [$status_code, $content];
  }

}
