<?php

namespace Drupal\user_provisioning\ProviderSpecific\Factory;

use Drupal\user_provisioning\moUserProvisioningConstants;
use Drupal\user_provisioning\ProviderSpecific\APIHandler\UserAPIHandler\moUserAzureAPIHandler;
use Drupal\user_provisioning\ProviderSpecific\APIHandler\UserAPIHandler\moUserOktaAPIHandler;
use Drupal\user_provisioning\ProviderSpecific\APIHandler\UserAPIHandler\moUserSCIMAPIHandler;
use Drupal\user_provisioning\ProviderSpecific\Parsers\UserParser\moUserAzureParser;
use Drupal\user_provisioning\ProviderSpecific\Parsers\UserParser\moUserOktaParser;
use Drupal\user_provisioning\ProviderSpecific\Parsers\UserParser\moUserSCIMParser;
use Drupal\user_provisioning\ProviderSpecific\ResponseProcessor\UserResponseProcessor\moUserAzureResponseProcessor;
use Drupal\user_provisioning\ProviderSpecific\ResponseProcessor\UserResponseProcessor\moUserOktaResponseProcessor;
use Drupal\user_provisioning\ProviderSpecific\ResponseProcessor\UserResponseProcessor\moUserSCIMResponseProcessor;

/**
 *
 */
class moUserFactory implements moResourceFactoryInterface {

  private string $app_name;

  /**
   *
   */
  public function __construct() {
    $app_name = \Drupal::config('user_provisioning.settings')->get('mo_user_provisioning_configured_application');
    if (empty($app_name)) {
      // FIXME uncomment the above line after saving the configured application name.
      $app_name = moUserProvisioningConstants::DEFAULT_APP;
    }
    $this->app_name = $app_name;
  }

  /**
   * {@inheritDoc}
   * */
  public function getAPIHandler() {
    if ($this->app_name == moUserProvisioningConstants::DEFAULT_APP) {
      return new moUserSCIMAPIHandler();
    }
    elseif ($this->app_name == moUserProvisioningConstants::AZURE_AD) {
      return new moUserAzureAPIHandler();
    }
    elseif ($this->app_name == moUserProvisioningConstants::OKTA) {
      return new moUserOktaAPIHandler();
    }
  }

  /**
   * {@inheritDoc}
   * */
  public function getParser() {
    if ($this->app_name == moUserProvisioningConstants::DEFAULT_APP) {
      return new moUserSCIMParser();
    }
    elseif ($this->app_name == moUserProvisioningConstants::AZURE_AD) {
      return new moUserAzureParser();
    }
    elseif ($this->app_name == moUserProvisioningConstants::OKTA) {
      return new moUserOktaParser();
    }
  }

  /**
   * {@inheritDoc}
   * */
  public function getResponseProcessor() {
    if ($this->app_name == moUserProvisioningConstants::DEFAULT_APP) {
      return new moUserSCIMResponseProcessor();
    }
    elseif ($this->app_name == moUserProvisioningConstants::AZURE_AD) {
      return new moUserAzureResponseProcessor();
    }
    elseif ($this->app_name == moUserProvisioningConstants::OKTA) {
      return new moUserOktaResponseProcessor();
    }
  }

}
