<?php

namespace Drupal\user_provisioning;

use Drupal\Core\Config\ImmutableConfig;

/**
 *
 */
class moUserProvisioningSupport {

  private ImmutableConfig $config;
  public $email;
  public $phone;
  public $query;
  public $query_type;
  public $trial_method;
  public $module_name;
  public $module_version;
    /**
     * @var mixed|string
     */
    private mixed $provider;

    /**
   *
   */
  public function __construct($email, $phone, $query, $query_type, $trial_method = '',$provider = '', $module_name = 'User Provisioning', $module_version = NULL) {
    $this->email = $email;
    $this->phone = $phone;
    $this->query = $query;
    $this->query_type = $query_type;
    $this->trial_method = $trial_method;
    $this->module_name = $module_name;
    $this->module_version = $module_version;
    $this->config = \Drupal::config('user_provisioning.settings');
    $this->provider = $provider;
  }

  /**
   * This function is written for sending the Support query.
   *
   * @return bool
   */
  public function sendSupportQuery() {
    $modules_info = \Drupal::service('extension.list.module')->getExtensionInfo('user_provisioning');
    $modules_version = $this->module_version == NULL ? $modules_info['version'] : $this->module_version;

    if ($this->query_type == 'Trial Request' || $this->query_type == 'Call Request' || $this->query_type == 'Contact Support') {

      $url = moUserProvisioningConstants::MO_NOTIFY_SEND;

      $request_for = $this->query_type == 'Trial Request' ? 'Trial' : 'Support';

      $subject = $request_for . ' request for Drupal-' . \DRUPAL::VERSION . ' ' . $this->module_name . ' Module | ' . $modules_version;
      $this->query = $request_for . ' requested for - ' . $this->query;

      $customerKey = $this->config->get('mo_user_provisioning_customer_id');
      $apikey = $this->config->get('mo_user_provisioning_customer_api_key');

      if ($customerKey == '') {
        $customerKey = "16555";
        $apikey = "fFd2XcvTGDemZvbw1bcUesNJWEqKbbUq";
      }

      $currentTimeInMillis = self::getTimestamp();
      $stringToHash = $customerKey . $currentTimeInMillis . $apikey;
      $hashValue = hash("sha512", $stringToHash);

      if ($this->query_type == 'Contact Support') {
        $content = '<div >Hello, <br><br>Company :<a href="' . $_SERVER['SERVER_NAME'] . '" target="_blank" >' . $_SERVER['SERVER_NAME'] . '</a><br><br><strong>Support needed for: </strong>' . $this->phone . '<br><br>Email:<a href="mailto:' . $this->email . '" target="_blank">' . $this->email . '</a><br><br>Query:[DRUPAL ' . moUserProvisioningUtilities::mo_get_drupal_core_version() . ' | ' . $this->module_name . ' Free | ' . $modules_version . ' | PHP ' . phpversion() . ' ] ' . $this->query . '</div>';
      }
      elseif ($this->query_type == 'Trial Request') {
        $content = '<div >Hello, <br><br>Company :<a href="' . $_SERVER['SERVER_NAME'] . '" target="_blank" >' . $_SERVER['SERVER_NAME'] . '</a><br><br>Trial Needed for: ' . $this->trial_method . '<br><br>Provider Name: ' . $this->provider . '<br><br>Email: <a href="mailto:' . $this->email . '" target="_blank">' . $this->email . '</a><br><br>Query:[DRUPAL ' . moUserProvisioningUtilities::mo_get_drupal_core_version() . ' | ' . $this->module_name . ' Free | PHP ' . phpversion() . ' | ' . $modules_version . ' ] ' . $this->query . '</div>';
      }
      else {
        $content = '<div >Hello, <br><br>Company :<a href="' . $_SERVER['SERVER_NAME'] . '" target="_blank" >' . $_SERVER['SERVER_NAME'] . '</a><br><br>Phone Number: ' . $this->phone . '<br><br>Email:<a href="mailto:' . $this->email . '" target="_blank">' . $this->email . '</a><br><br>Query:[DRUPAL ' . moUserProvisioningUtilities::mo_get_drupal_core_version() . ' | ' . $this->module_name . ' Free | PHP ' . phpversion() . ' | ' . $modules_version . ' ] ' . $this->query . '</div>';
      }

      $fields = [
        'customerKey' => $customerKey,
        'sendEmail' => TRUE,
        'email' => [
          'customerKey' => $customerKey,
          'fromEmail' => $this->email,
          'fromName' => 'miniOrange',
          'toEmail' => moUserProvisioningConstants::SUPPORT_EMAIL,
          'toName' => moUserProvisioningConstants::SUPPORT_EMAIL,
          'subject' => $subject,
          'content' => $content,
        ],
      ];

      $header = [
        'Content-Type' => 'application/json',
        'Customer-Key' => $customerKey,
        'Timestamp' => $currentTimeInMillis,
        'Authorization' => $hashValue,
      ];

    }
    else {

      $this->query = '[Drupal ' . \DRUPAL::VERSION . $this->module_name . ' Module | PHP ' . phpversion() . ' | ' . $modules_version . '] ' . $this->query;
      $fields = [
        'company' => $_SERVER['SERVER_NAME'],
        'email' => $this->email,
        'phone' => $this->phone,
        'ccEmail' => moUserProvisioningConstants::SUPPORT_EMAIL,
        'query' => $this->query,
      ];

      $url = moUserProvisioningConstants::CONTACT_US;

      $header = [
        'Content-Type' => 'application/json',
        'charset' => 'UTF-8',
        'Authorization' => 'Basic',
      ];
    }

    $field_string = json_encode($fields);
    $mo_user_provisioning_customer = new moUserProvisioningCustomer(NULL, NULL, NULL, NULL);
    $response = $mo_user_provisioning_customer->callService($url, $field_string, $header);

    return TRUE;
  }

  /**
   * This function is written to get the timestamp.
   *
   * @return string
   */
  public static function getTimestamp() {
    $url = moUserProvisioningConstants::GET_TIMESTAMP;
    $mo_user_provisioning_customer = new moUserProvisioningCustomer(NULL, NULL, NULL, NULL);
    $content = $mo_user_provisioning_customer->callService($url, [], []);

    if (empty($content)) {
      $currentTimeInMillis = round(microtime(TRUE) * 1000);
      $currentTimeInMillis = number_format($currentTimeInMillis, 0, '', '');
    }
    return empty($content) ? $currentTimeInMillis : $content;
  }

}
