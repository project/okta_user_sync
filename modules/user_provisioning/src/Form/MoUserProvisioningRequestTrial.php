<?php

namespace Drupal\user_provisioning\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user_provisioning\moUserProvisioningConstants;
use Drupal\user_provisioning\moUserProvisioningSupport;
use Drupal\user_provisioning\moUserProvisioningUtilities;

/**
 *
 */
class MoUserProvisioningRequestTrial extends FormBase {
  private ImmutableConfig $config;
  protected $messenger;

  /**
   *
   */
  public function __construct() {
    $this->config = \Drupal::config('user_provisioning.settings');
    $this->messenger = \Drupal::messenger();
  }

  /**
   *
   */
  public function getFormId() {
    return 'user_provisioning_request_trial';
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state, $options = NULL) {

    $form['#prefix'] = '<div id="modal_example_form">';
    $form['#suffix'] = '</div>';
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $user_email = $this->config->get('mo_user_provisioning_customer_email');

    $form['mo_user_provisioning_trial_email_address'] = [
      '#type' => 'email',
      '#title' => t('Email'),
      '#default_value' => $user_email,
      '#required' => TRUE,
      '#attributes' => ['placeholder' => t('Enter your email'), 'style' => 'width:99%;margin-bottom:1%;'],
    ];

    $form['mo_user_provisioning_trial_method'] = [
      '#type' => 'select',
      '#title' => t('Trial Method'),
      '#attributes' => ['style' => 'width:99%;height:30px;margin-bottom:1%;'],
      '#options' => [
        'Drupal ' . moUserProvisioningUtilities::mo_get_drupal_core_version() . ' SCIM Client' => t('Drupal ' . moUserProvisioningUtilities::mo_get_drupal_core_version() . ' SCIM CLIENT - Sync information from Drupal'),
        'Drupal ' . moUserProvisioningUtilities::mo_get_drupal_core_version() . ' SCIM SERVER' => t('Drupal ' . moUserProvisioningUtilities::mo_get_drupal_core_version() . ' SCIM SERVER - Sync information into Drupal'),
        'Drupal ' . moUserProvisioningUtilities::mo_get_drupal_core_version() . ' PROVIDER SPECIFIC PROVISIONING' => t('Drupal ' . moUserProvisioningUtilities::mo_get_drupal_core_version() . ' Two-way sync between Drupal and provider'),
        'Not Sure' => t('Not Sure (Want to schedule a meeting with Drupal developer for assistance.)'),
      ],
    ];

      $form['mo_user_provisioning_trial_provider'] = [
          '#type' => 'textarea',
          '#rows' => 1,
          '#required' => TRUE,
          '#title' => t('Provider/Application'),
          '#attributes' => ['placeholder' => t('Enter provider name with which you want to sync.'), 'style' => 'width:99%;'],
      ];

    $form['mo_user_provisioning_trial_description'] = [
      '#type' => 'textarea',
      '#rows' => 3,
      '#required' => TRUE,
      '#title' => t('Description'),
      '#attributes' => ['placeholder' => t('Describe your use case here!'), 'style' => 'width:99%;'],
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button--primary',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $form;
  }

  /**
   *
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();
    $response = new AjaxResponse();
    // If there are any form errors, AJAX replace the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#modal_example_form', $form));
    }
    else {

      $requested_trial_version = $form_values['mo_user_provisioning_trial_method'];
      $email = $form_values['mo_user_provisioning_trial_email_address'];
      $provider = $form_values['mo_user_provisioning_trial_provider'];
      $query = $requested_trial_version . ' : ' . trim($form_values['mo_user_provisioning_trial_description']);
      $query_type = 'Trial Request';

      $support = new moUserProvisioningSupport($email, '', $query, $query_type, $requested_trial_version, $provider);
      $support_response = $support->sendSupportQuery();

      $this->messenger->addStatus(t('Success! Trial query successfully sent. We will provide you with the trial version shortly.'));
      $response->addCommand(new RedirectCommand(Url::fromRoute('user_provisioning.provisioning')->toString()));
    }
    return $response;
  }

  /**
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
