<?php

namespace Drupal\user_provisioning\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user_provisioning\moUserProvisioningUtilities;
use Psr\Log\LoggerInterface;

/**
 *
 */
class MoUserProvisioningUserExport extends FormBase {
  private $base_url;
  private ImmutableConfig $config;
  private Config $config_factory;
  private LoggerInterface $logger;

  /**
   *
   */
  public function __construct() {
    $this->base_url = \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getBaseUrl();
    $this->config = \Drupal::config('user_provisioning.settings');
    $this->config_factory = \Drupal::configFactory()->getEditable('user_provisioning.settings');
    $this->logger = \Drupal::logger('user_provisioning');
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return "mo_export";
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['mo_user_provision_add_css'] = [
      '#attached' => [
        'library' => [
          'user_provisioning/user_provisioning.admin',
          'core/drupal.dialog.ajax',
        ],
      ],
    ];

    $form['markup_top'] = [
      '#markup' => '<!--<br><br>--><div class="mo_user_provisioning_sp_font_for_sub_heading"><strong>Export Users</strong></div><hr/><p>
                                Select the users attributes that you want to export.</p>',
    ];

    $form['message'] = [
      '#markup' => '<div id="tester"></div>',
    ];

    $user_attribute = moUserProvisioningUtilities::customUserFields();
    unset($user_attribute['']);
    $user_attribute = array_keys($user_attribute);

    $form['mo_user_provisioning_export_attribute'] = [
      '#type' => 'table',
      '#attributes' => ['style' => 'border-collapse: separate;'],
    ];

    $count = 0;
    for ($i = 0; $i < (int) sizeof($user_attribute) / 3; $i++) {
      if ($i * 3 + 1 <= sizeof($user_attribute)) {
        $form['mo_user_provisioning_export_attribute'][$i][$user_attribute[$count]] = [
          '#type' => 'checkbox',
          '#disabled' => $user_attribute[$count] != 'name' && $user_attribute[$count] != 'mail',
          '#default_value' => $user_attribute[$count] == 'name' || $user_attribute[$count] == 'mail',
          '#title' => $user_attribute[$count++],
        ];
      }
      if ($i * 3 + 2 <= sizeof($user_attribute)) {
        $form['mo_user_provisioning_export_attribute'][$i][$user_attribute[$count]] = [
          '#type' => 'checkbox',
          '#disabled' => $user_attribute[$count] != 'name' && $user_attribute[$count] != 'mail',
          '#default_value' => $user_attribute[$count] == 'name' || $user_attribute[$count] == 'mail',
          '#title' => $user_attribute[$count++],
        ];
      }
      if ($i * 3 + 3 <= sizeof($user_attribute)) {
        $form['mo_user_provisioning_export_attribute'][$i][$user_attribute[$count]] = [
          '#type' => 'checkbox',
          '#disabled' => $user_attribute[$count] != 'name' && $user_attribute[$count] != 'mail',
          '#default_value' => $user_attribute[$count] == 'name' || $user_attribute[$count] == 'mail',
          '#title' => $user_attribute[$count++],
        ];
      }
    }

    $form['mo_user_provisioning_file_type_option'] = [
      '#type' => 'radios',
      '#title' => t("Please select file extension you want to download"),
      '#attributes' => ["style" => "display:inline"],
      '#default_value' => 'json',
      '#options' => ['json' => t('.json'), 'csv' => t('.csv')],
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button--primary',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitForm'],
        'event' => 'click',
      ],
    ];

    $form['mo_user_provisioning_div_close'] = [
      '#markup' => '<br><br></div>',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $command = new CloseModalDialogCommand();

    $form_values = $form_state->getValue('mo_user_provisioning_export_attribute');
    $file_extension = $form_state->getValue('mo_user_provisioning_file_type_option');

    $user_attribute = [];
    foreach ($form_state->getValues()['mo_user_provisioning_export_attribute'] as $key => $value) {
      foreach ($value as $field_name => $field_value) {
        if (($field_name == 'name' || $field_name == 'mail') && $field_value) {
          $user_attribute[$field_name] = $field_value;
        }
      }
    }

    if (empty($user_attribute)) {
      $response->addCommand(new ReplaceCommand('#tester', '<span style="color: red">Please select at least one attribute.</span>'));
      return $response;
    }

    $this->config_factory->set('mo_user_provisioning_export_config', $user_attribute)->save();
    $this->config_factory->set('mo_user_provisioning_file_extension', $file_extension)->save();

    $response->addCommand($command);
    $response->addCommand(new RedirectCommand(Url::fromRoute('user_provisioning.export_users',)->toString()));

    return $response;

  }

}
