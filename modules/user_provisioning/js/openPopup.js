if (!getCookie('popup_opened')) {
    setTimeout(() => {
        setCookie('popup_opened', true, 2);
        const current_url = document.URL;
        const trial_path = current_url.split('user_provisioning');
        Drupal.ajax({url: trial_path[0]+'user_provisioning/requestTrial'}).execute();
    }, 5000);
}

function setCookie(name, value, days) {
    const date = new Date(Date.now() + days * 24 * 60 * 60 * 1000);
    document.cookie = `${name}=${value || ''}; expires=${date.toUTCString()}; path=/`;
}

function getCookie(name) {
    const nameEQ = `${name}=`;
    for (const c of document.cookie.split(';')) {
        if (c.trim().startsWith(nameEQ)) {
            return c.trim().substring(nameEQ.length);
        }
    }
    return null;
}